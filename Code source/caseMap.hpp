#ifndef CASEMAP_HPP
#define CASEMAP_HPP

#include "general.hpp"

#include "fourmilliere.hpp"
#include "pheromone.hpp"
#include "fourmi.hpp"

class Fourmi;
class Fourmilliere;
class Pheromone;
class CaseMap;

class CaseMap
{
    private:


        int iCase;
        int jCase;

        PaysageCase paysageCase;
        int bouffeQuantity;


        Fourmilliere* fourmilliereCase;

        int nbrFourmisPresentes;
        Fourmi* listeFourmisPresentes[NBRMAXFOURMISPARCASE];
        list<Pheromone> listePheromoneCase[NBRCASESVOISINES];

        CaseMap** listeCasesVoisines;

    public:
        CaseMap(int, int, PaysageCase, int);
        ~CaseMap();


        bool toutEstOkSurCaseDebutAction(); // vérifie notamment si toutes les fourmis présentes sont bel et bien à l'endroit qu'elles pensent

        void ajouterPheromone(Pheromone, Direction);
        void faireVieillirPheromones();

        int ajouterFourmi(Fourmi*); // si on a réussi à trouver un slot, alors on la fait automatiquement communiquer/partager bouffe avec les fourmis présentes
        bool retirerFourmi(Fourmi*);
        
        Fourmilliere* GetFourmilliereCase();
        void SetFourmilliereCase(Fourmilliere*);


        int GetNbrFourmisPresentes();
        Fourmi** GetListeFourmisPresentes();



        CaseMap* GetCaseVoisine(int);
        void SetCaseVoisine(CaseMap*, int);

        int GetNbrPheromones();
        int GetNbrPheromones(TypePheromone);
        int GetNbrPheromones(TypePheromone, Direction);

        int GetNbrPheromonesAmis(TypePheromone, int);
        int GetNbrPheromonesAmis(TypePheromone, Direction, int);

        PaysageCase GetPaysageCase();
        int GetBouffeQuantity();
        void SetBouffeQuantity(int);

};




#endif
