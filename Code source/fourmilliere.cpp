#include "fourmilliere.hpp"

Fourmilliere::Fourmilliere(int nouveauIndiceFourmilliere, CaseMap* caseNouvelleFourmilliere)
{
    CRactionsFourmilliere = "";

    indiceFourmilliere = nouveauIndiceFourmilliere;
    caseMapFourmilliere = caseNouvelleFourmilliere;

    cout << "je créé une fourmilliere\n";

    niveauBouffeDansFourmilliere = STARTERPACKDEBOUFFEFOURMILLIERE;
    reineVivante = true;

    nbrFourmisLoyales = 0;
    listeFourmisLoyales = new Fourmi *[NBRMAXFOURMISPARFOURMILLIERE];
    for(int i = 0; i < NBRMAXFOURMISPARFOURMILLIERE; i++)
    {
        listeFourmisLoyales[i] = nullptr;
    }

    nbrFourmisPresentes = 0;
    listeFourmisPresentes = new bool[NBRMAXFOURMISPARFOURMILLIERE];
    for(int i = 0; i < NBRMAXFOURMISPARFOURMILLIERE; i++)
    {
        listeFourmisPresentes[i] = false;
    }
    
}

Fourmilliere::~Fourmilliere()
{
    for(int i = 0; i < NBRMAXFOURMISPARFOURMILLIERE; i++)
    {
        if(listeFourmisLoyales[i] != nullptr)
        {
            decesFourmi(listeFourmisLoyales[i]);
        }
    }

    delete [] listeFourmisLoyales;
    delete [] listeFourmisPresentes;
    cout << "je détruis une fourmilliere\n";
}



bool Fourmilliere::toutEstOkDansFourmilliereDebutAction() // appel uniquement depuis bool Fourmi::toutEstOkDebutAction() donc on part du principe que les tests de cette fonction-là ont été validés
{
    bool toutEstOkdansFourmilliere = false;

    if(indiceFourmilliere >= 0 && indiceFourmilliere < NBRFOURMILLIERES && niveauBouffeDansFourmilliere >= 0 /*&& niveauBouffeDansFourmilliere <= MAXFOODLEVELFOURMILLIERE*/)
    {
        if(nbrFourmisLoyales >= 0 && nbrFourmisLoyales <= NBRMAXFOURMISPARFOURMILLIERE && nbrFourmisPresentes >= 0 && nbrFourmisPresentes <= NBRMAXFOURMISPARFOURMILLIERE)
        {
            if(caseMapFourmilliere != nullptr && listeFourmisLoyales != nullptr && listeFourmisPresentes != nullptr)
            {
                if(caseMapFourmilliere->GetFourmilliereCase() == this)
                {
                    bool pasDeProbleme = true;

                    int compteurFourmisLoyales = 0;
                    int compteurFourmisPresentes = 0;

                    int i = 0;
                    while(pasDeProbleme && i < NBRMAXFOURMISPARFOURMILLIERE)
                    {
                        pasDeProbleme = false;

                        if(listeFourmisPresentes[i])
                        {
                            compteurFourmisPresentes++;
                        }

                        if(listeFourmisLoyales[i] != nullptr)
                        {
                            if(listeFourmisLoyales[i]->GetIndiceFourmilliere() == indiceFourmilliere)
                            {
                                if((listeFourmisPresentes[i] && listeFourmisLoyales[i]->GetPositionFourmiSurCase() == -1) || (!listeFourmisPresentes[i] && listeFourmisLoyales[i]->GetPositionFourmiSurCase() != -1))
                                {
                                    compteurFourmisLoyales++;
                                    pasDeProbleme = true;
                                }
                                else
                                {
                                    cout << "fourmilliere et fourmi loyale ne sont pas d'accord sur le fait que la fourmi soit présente ou pas\n";
                                }
                            }
                            else
                            {
                                cout << "Une fourmilliere pense qu'une fourmi non loyale est loyale !! \n";
                            }
                        }
                        else
                        {
                            if(!listeFourmisPresentes[i])
                            {
                                pasDeProbleme = true;
                            }
                            else
                            {
                                cout << "Une fourmilliere pense qu'une fourmi qui n'existe pas est présente !! \n";
                            }
                        }
                        i++;
                    }

                    if(pasDeProbleme && compteurFourmisLoyales == nbrFourmisLoyales && compteurFourmisPresentes == nbrFourmisPresentes)
                    {
                        toutEstOkdansFourmilliere = true;
                    }
                    else
                    {
                        cout << "nbr fourmis loyales : " << compteurFourmisLoyales << " nbr fourmis présentes " << compteurFourmisPresentes << "\n";
                        cout << "Probleme lors du passage en revue des fourmis de la fourmilliere ou alors le compte n'est pas bon ! \n";
                    }
                }
                else
                {
                    cout << "La fourmilliere pense etre sur une case mais elle ne l'est pas\n";
                }
            }
            else
            {
                cout << "Probleme dans les parametres basiques de la fourmilliere (il y a des nullptr)\n";
            }
        }
        else
        {
            cout << "nbr fourmis loyales : " << nbrFourmisLoyales << " nbr fourmis présentes " << nbrFourmisPresentes << "\n";
            cout << "Probleme dans les parametres basiques de la fourmilliere (nbrFourmisLoyales ou nbrFourmisPresentes bizarre)\n";
        }
    }
    else
    {
        cout << "indice fourmilliere : " << indiceFourmilliere << "; niveauBouffeFourmilliere : " << niveauBouffeDansFourmilliere << "\n";
        cout << "Probleme dans les parametres basiques de la fourmilliere (indiceFourmilliere ou niveauBouffeDansFourmilliere bizarre)\n";

    }


    return toutEstOkdansFourmilliere;
}





void Fourmilliere::playTurnFourmilliere()
{
    
    if(reineVivante)
    {
        CRactionsFourmilliere += "\nNiveau bouffe : " + to_string(niveauBouffeDansFourmilliere) + "; Nbr fourmis loyales : " + to_string(nbrFourmisLoyales) + "; Nbr fourmis présentes : " + to_string(nbrFourmisPresentes) + "; ";

        if(niveauBouffeDansFourmilliere - BOUFFEPOURNOURRIRREINE >= 0)
        {
            niveauBouffeDansFourmilliere -= BOUFFEPOURNOURRIRREINE; // La reine se nourrit
        }
        else
        {
            niveauBouffeDansFourmilliere = 0; // la reine se nourrit jusqu'à ce qu'il n'y ait plus de bouffe mais elle meurt puisque ce n'était pas assez.
            reineVivante = false;
            CRactionsFourmilliere += "Reine vient de mourir !! \n";

            if(AFFICHERINFOSDEVELOPPEUR)
        {
            cout << "Reine vient de mourir !! \n";
        }
        }
    }
    
    //nourrirFourmis();

    EtatFourmilliere niveauFamineFourmilliere = GetEtatFourmilliere();

    if(reineVivante)
    {
        CRactionsFourmilliere += "Etat Fourmilliere : " + to_string(niveauFamineFourmilliere) + "\n";
    }

    if(niveauFamineFourmilliere == INCROYABLEGOPONDRE )
    {
        int maxOrdrePonte = (niveauBouffeDansFourmilliere - NIVEAUBOUFFEPARFOURMILOYALEPONTE * nbrFourmisLoyales - 1000) / BOUFFEPOURPONDREFOURMI;
        int ordrePonte = uniform_int(0, maxOrdrePonte);
        //ordrePonte = 0;

        for(int i = 0; i < ordrePonte; i++)
        {
            pondreFourmi();
        }

        if(AFFICHERINFOSDEVELOPPEUR)
        {
            cout << "nbr fourmis pondues : " << ordrePonte << "\n";
        }
        CRactionsFourmilliere += "nbr fourmis pondues : " + to_string(ordrePonte) +"\n";
    }
    //cout << "etat fourmilliere : " << niveauFamineFourmilliere << "\n";
}


bool Fourmilliere::fourmiEstLoyale(Fourmi* fourmi)
{
    bool estLoyale = false;

    int indiceListeLoyaleFourmi = fourmi->GetIndiceListeFourmisLoyalesFourmilliere();
    if(listeFourmisLoyales[indiceListeLoyaleFourmi] == fourmi)
    {
        estLoyale = true;
    }

    return estLoyale;
}


bool Fourmilliere::fourmiEstLoyaleEtPresente(Fourmi* fourmi)
{
    bool estLoyaleEtPresente = false;

    if(fourmi != nullptr)
    {
        int indiceListeLoyaleFourmi = fourmi->GetIndiceListeFourmisLoyalesFourmilliere();
        if(listeFourmisLoyales[indiceListeLoyaleFourmi] == fourmi)
        {
            if(listeFourmisPresentes[indiceListeLoyaleFourmi] == true)
            {
                estLoyaleEtPresente = true;
            }
        }
    }

    return estLoyaleEtPresente;
}



void Fourmilliere::nourrirFourmiInactive()
{
    if(niveauBouffeDansFourmilliere - RESTERINACTIVE >= 0)
    {
        niveauBouffeDansFourmilliere -= RESTERINACTIVE;
    }
}




void Fourmilliere::pondreFourmi()
{
    if(niveauBouffeDansFourmilliere - BOUFFEPOURPONDREFOURMI >= 0)
    {
        if(nbrFourmisLoyales < NBRMAXFOURMISPARFOURMILLIERE)
        {
            bool slotTrouved =  false;
            int i = 0;
            while(!slotTrouved && i < NBRMAXFOURMISPARFOURMILLIERE)
            {
                if(listeFourmisLoyales[i] == nullptr)
                {
                    Fourmi * nouvelleFourmi = new Fourmi(i, indiceFourmilliere, caseMapFourmilliere);
                    niveauBouffeDansFourmilliere -= BOUFFEPOURPONDREFOURMI; // On dépense la nourriture que coûte une ponte.

                    listeFourmisLoyales[i] = nouvelleFourmi;
                    listeFourmisPresentes[i] = true; //La fourmi naît dans la fourmilliere du coup elle est présente
                    
                    nbrFourmisLoyales++;
                    nbrFourmisPresentes++;

                    slotTrouved = true;
                }
                i++;
            }
            if(!slotTrouved)
            {
                cout << "pas trouvé de slot disponible pour pondre fourmi alors que théoriquement y a assez de place\n";
            }
        }
        else
        {
            cout << "Trop de fourmis : on ne peut pas en pondre plus\n";
        }
    }
    else
    {
        cout << "Si on effectue la ponte, alors le niveau de bouffe va passer en négatif !!! La ponte a donc été annulée \n";
    }
}



void Fourmilliere::decesFourmi(Fourmi* fourmiMorte)
{
    int bouffeCadavre = fourmiMorte->GetJabotSocialLevel() + fourmiMorte->GetCarryLevel(); // peut être négatif quand une fourmi est affamée

    if(bouffeCadavre > 0) // quand une fourmi meurt elle se transforme en bouffe à l'endroit où elle est morte (case et fourmillière inclue)
    {
        if(fourmiMorte->GetPositionFourmiSurCase() == -1) // si la fourmi meurt dans la fourmilliere, la bouffe représentée par son cadavre est directement placée dans les réserves de la fourmilliere.
        {
            niveauBouffeDansFourmilliere += bouffeCadavre;
        }
        else // si la fourmi meurt à l'extérieur, alors son cadavre va augmenter la quantityBouffe de la case
        {
            fourmiMorte->GetCaseActuelle()->SetBouffeQuantity(fourmiMorte->GetCaseActuelle()->GetBouffeQuantity() + bouffeCadavre);
        }
    }


    int indiceListeLoyaleFourmiMorte = fourmiMorte->GetIndiceListeFourmisLoyalesFourmilliere();

    
    listeFourmisLoyales[indiceListeLoyaleFourmiMorte] = nullptr;
    nbrFourmisLoyales--;

    if(fourmiMorte->GetPositionFourmiSurCase() == -1) // si la fourmi est dans la fourmilliere
    {
        listeFourmisPresentes[indiceListeLoyaleFourmiMorte] = false;
        nbrFourmisPresentes--;
    }
    else // si elle est sur une case
    {
        if(!fourmiMorte->GetCaseActuelle()->retirerFourmi(fourmiMorte))
        {
            cout << "la fourmi est morte sur une case mais n'a pas réussi à être retirée de la case\n";
        }
    }

    delete fourmiMorte;
}


bool Fourmilliere::entreeFourmi(Fourmi * fourmiEntrante)
{
    bool fourmiEntered = false;

    if(fourmiEntrante != nullptr)
    {
        int indiceListeLoyaleFourmiEntrante = fourmiEntrante->GetIndiceListeFourmisLoyalesFourmilliere();
        if(listeFourmisLoyales[indiceListeLoyaleFourmiEntrante] == fourmiEntrante)
        {    
            if(listeFourmisPresentes[indiceListeLoyaleFourmiEntrante] == false)
            {
                //if(fourmiEntrante->GetJabotSocialLevel() >= 0 && fourmiEntrante->GetJabotSocialLevel() <= MAXJABOTSOCIALLEVELFOURMI && fourmiEntrante->GetCarryLevel() >= 0 && fourmiEntrante->GetCarryLevel() <= MAXCARRYLEVELFOURMI) 
                //{
                    listeFourmisPresentes[indiceListeLoyaleFourmiEntrante] = true;
                    nbrFourmisPresentes++;

                    // La fourmi entrante dépose automatiquement dans les réserves de la fourmilliere toute la bouffe qu'elle transporte.
                    if(fourmiEntrante->GetJabotSocialLevel() >= 0)
                    {
                        niveauBouffeDansFourmilliere += fourmiEntrante->GetJabotSocialLevel();
                        totalcollectetfood+= fourmiEntrante->GetJabotSocialLevel();
                    }

                    niveauBouffeDansFourmilliere += fourmiEntrante->GetCarryLevel();
                     totalcollectetfood+= fourmiEntrante->GetCarryLevel();


                    fourmiEntered = true;

            }
            else
            {
                cout << "La fourmi essaie de rentrer alors qu'elle est déjà présente dans la fourmilliere\n";
            }
        }
        else
        {
            cout << "La fourmi essaie de rentrer dans une fourmillière étrangère\n";
        }
    }
    else
    {
        cout << "La fourmi entrante est un nullptr !!\n";
    }

    return fourmiEntered;
}


bool Fourmilliere::sortieFourmi(Fourmi * fourmiSortante)
{
    bool fourmisortie;

    if(fourmiSortante != nullptr)
    {
        int indiceListeLoyaleFourmiSortante = fourmiSortante->GetIndiceListeFourmisLoyalesFourmilliere();
        if(listeFourmisLoyales[indiceListeLoyaleFourmiSortante] == fourmiSortante)
        {    
            if(listeFourmisPresentes[indiceListeLoyaleFourmiSortante] == true)
            {
                listeFourmisPresentes[indiceListeLoyaleFourmiSortante] = false;
                nbrFourmisPresentes--;
                niveauBouffeDansFourmilliere = niveauBouffeDansFourmilliere - fourmiSortante->GetJabotSocialLevel() - ENTREEOUSORTIEFOURMILLIERE; // La fourmi a pris ce qu'il lui fallait. Là on se contente d'enlever de la réserve ce qu'elle a amené
                fourmisortie = true;
            }
            else
            {
                cout << "La fourmi n'était pas présente dans la fourmilliere mais essaie quand meme de sortir\n";
            }
        }
        else
        {
            cout << "La fourmi essaie de sortir d'une une fourmillière étrangère\n";
        }
    }
    else
    {
        cout << "La fourmi sortante est un nullptr !!\n";
    }

    return fourmisortie;
}


EtatFourmilliere Fourmilliere::GetEtatFourmilliere()
{
    EtatFourmilliere niveauFamineFourmilliere;

    if(reineVivante == false)
    {
        niveauFamineFourmilliere = REINEMORTE;
    }
    else if(niveauBouffeDansFourmilliere < NBRTOURSSURVIEULTIMEREINE * BOUFFEPOURNOURRIRREINE)
    {
        niveauFamineFourmilliere = SURVIEULTIME;
    }
    else if(niveauBouffeDansFourmilliere < NIVEAUBOUFFEPARFOURMILOYALEFAMINE * nbrFourmisLoyales)
    {
        niveauFamineFourmilliere = FAMINE;
    }
    else if(niveauBouffeDansFourmilliere <= NIVEAUBOUFFEPARFOURMILOYALEPONTE * nbrFourmisLoyales)
    {
        niveauFamineFourmilliere = TOUTVABIEN;
    }
    else
    {
        niveauFamineFourmilliere = INCROYABLEGOPONDRE;
    }

    return niveauFamineFourmilliere;
}

/***************************************************************************//**
 * Détermine la quantité de nourriture que la fourmi peut prendre quand elle part
 ******************************************************************************/
int Fourmilliere::quantityProvisionsAutorisedPourSortie()
{
    int quantityAutorised = 0;
    EtatFourmilliere niveauFamineFourmilliere = GetEtatFourmilliere();

    if(niveauFamineFourmilliere == INCROYABLEGOPONDRE)
    {
        quantityAutorised = MAXJABOTSOCIALLEVELFOURMI;
    }
    else if(niveauFamineFourmilliere == TOUTVABIEN)
    {
        quantityAutorised = MAXJABOTSOCIALLEVELFOURMI;
    }
    else if(niveauFamineFourmilliere == FAMINE)
    {
        quantityAutorised = NBRTOURSPROVISIONSEXPLORATIONFAMINE * NBRPAFOURMI * BOUGERDE1PA;
    }
    else if(niveauFamineFourmilliere == URGENCEFAMINE)
    {
        quantityAutorised = NBRTOURSPROVISIONSSURVIEULTIME * (NBRPAFOURMI + BONUSNBRPAFOURMIDOPED) * BOUGERDE1PA;
    }
    else if(niveauFamineFourmilliere == SURVIEULTIME)
    {
        quantityAutorised = NBRTOURSSURVIEULTIMEREINE * BOUFFEPOURNOURRIRREINE;
    }
    else if(niveauFamineFourmilliere == REINEMORTE)
    {
        quantityAutorised = MAXJABOTSOCIALLEVELFOURMI;
    }
    else
    {
        cout << "Grosse erreur de niveauFamine\n";
    }

    if(niveauBouffeDansFourmilliere - quantityAutorised < 0) 
    {
        quantityAutorised = niveauBouffeDansFourmilliere;
    }

    return quantityAutorised; // ce sera retiré des réserves dans Fourmilliere::sortieFourmi()
}




//Getters



int Fourmilliere::GetIndiceFourmilliere(){return indiceFourmilliere;}
int Fourmilliere::GetNiveauBouffe(){return niveauBouffeDansFourmilliere;}

int Fourmilliere::GetNbrfourmisLoyales(){return nbrFourmisLoyales;}
Fourmi** Fourmilliere::GetListeFourmisLoyales(){return listeFourmisLoyales;}

int Fourmilliere::GetNbrfourmisPresentes(){return nbrFourmisPresentes;}
bool* Fourmilliere::GetListeFourmisPresentes(){return listeFourmisPresentes;}


