#ifndef DATA_HPP
#define DATA_HPP
#include <string>
#include <list>
class Map
{
	/*
	type

0 vide
1 obstacle
2 nourriture
3 fourmillière
4 fourmi

	info
1
2 niveau de nourriture
3 id de la fourmillère
4 id de la fourmi
	*/

	private :
		int map[20][20];

	public :
		int getValueAt(int x, int y);
		void setValueAt(int x, int y, int value);
		std::string Display();
		Map();
};


class Pheromone
{
	public :
		int getValue(int direction, int type);
		void setValue(int direction, int type, int value);
	private :
		int phAlert[4]; //0
		int phPathToAnthill[4]; //1
		int phPathToFood[4]; //2

};


class PheromoneMap
{
	public :
		int getValueAt(int x, int y, int direction, int type);
		void setValueAt(int x, int y, int direction, int type, int value);
		std::string Display();
		PheromoneMap();
		void LowerLevel();
	private :
		Pheromone map[20][20];


};


class Fourmi
{

};

class Fourmilliere
{
	
};

class GameManager
{
	public :
		Map objectType;
    	Map objectInfo;
    	PheromoneMap pheromoneMap;
		std::list<Fourmi> fourmiList;
		std::list<Fourmilliere> fourmilliereList;
		void CreateFood();
};

#endif