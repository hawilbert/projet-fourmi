#include "data.hpp"

int Map::getValueAt(int x, int y)
{
    return map[x][y];
}

void Map::setValueAt(int x, int y, int value)
{
    map[x][y] = value;
}

std::string Map::Display()
{
    std::string outString="";

    for (int i =0 ; i < 20; i++)
    {
         for (int j =0 ; j < 20; j++)
    {
    outString += std::to_string(getValueAt(i,j));   
        outString += "\t";
    }   


outString += "\n";
    }
    outString += "\n";
    
    return outString;
}

Map::Map()
{
    std::string outString="";
    
    for (int t = 0; t < 3; t++)
    {
    for (int i =0 ; i < 20; i++)
    {
         for (int j =0 ; j < 20; j++)
    {
setValueAt(i,j,0);   
    }   
    }
    }
}

void Pheromone::setValue(int direction, int type, int value)
{
    if (direction <= 3 && direction >=0)
    {
    switch(type)
    {
        case 0 :
            phAlert[direction] = value;
        break;
        case 1:
            phPathToAnthill[direction] = value;
        break;
        default:
            phPathToFood[direction] = value;
        break;
    }
    }
}

int Pheromone::getValue(int direction, int type)
{
    int outvalue;
    if (direction <= 3 && direction >=0)
    {
    switch(type)
    {
        case 0 :
            outvalue=phAlert[direction];
        break;
        case 1:
            outvalue=phPathToAnthill[direction];
        break;
        default:
            outvalue=phPathToFood[direction];
        break;
    }
    }
    return outvalue;
}

int PheromoneMap::getValueAt(int x, int y, int direction, int type)
{
    Pheromone ph = map[x][y];

    return ph.getValue(direction, type);
}

void PheromoneMap::setValueAt(int x, int y, int direction, int type, int value)
{
    Pheromone ph = map[x][y];

    ph.setValue(direction, type, value);

    map[x][y] = ph;
}

std::string PheromoneMap::Display()
{
    std::string outString="";
    
    for (int t = 0; t < 3; t++)
    {
    for (int i =0 ; i < 20; i++)
    {
         for (int j =0 ; j < 20; j++)
    {
        outString += std::to_string(getValueAt(i,j,0,t));    
        outString += std::to_string(getValueAt(i,j,1,t)); 
        outString += "\t";
    }   
        outString += "\n";
         for (int j =0 ; j < 20; j++)
    {
        outString += std::to_string(getValueAt(i,j,2,t));    
        outString += std::to_string(getValueAt(i,j,3,t)); 
        outString += "\t";
    }  

outString += "\n\n";
    }
    outString += "\n\n______________\n\n";
    }
    return outString;
}

PheromoneMap::PheromoneMap()
{

for (int i = 0; i< 20 ; i++)
{
 for (int j = 0; j< 20 ; j++)
{
setValueAt(i,j, 0, 0,0);
setValueAt(i,j, 1, 0,0);
setValueAt(i,j, 2, 0,0);
setValueAt(i,j, 3, 0,0);

setValueAt(i,j, 0, 1,0);
setValueAt(i,j, 1, 1,0);
setValueAt(i,j, 2, 1,0);
setValueAt(i,j, 3, 1,0);

setValueAt(i,j, 0, 2,0);
setValueAt(i,j, 1, 2,0);
setValueAt(i,j, 2, 2,0);
setValueAt(i,j, 3, 2,0);


}   

}

}

void PheromoneMap::LowerLevel()
{
    
    for (int t = 0; t < 3; t++)
    {
    for (int i =0 ; i < 20; i++)
    {
         for (int j =0 ; j < 20; j++)
    {
        setValueAt(i,j,0,t, getValueAt(i,j,0,t) - 1);    
        setValueAt(i,j,1,t, getValueAt(i,j,1,t) - 1);   
        setValueAt(i,j,2,t, getValueAt(i,j,2,t) - 1);   
        setValueAt(i,j,3,t, getValueAt(i,j,3,t) - 1);   
        
    }  
    }
    }

}


void GameManager::CreateFood()
{
    int i = std::rand()%20;
    int j = std::rand()%20;
    while(objectType.getValueAt(i,j) != 0)
    {
        i = std::rand()%20;
        j = std::rand()%20;
    }
    objectInfo.setValueAt(i,j,std::rand()%10);
    objectType.setValueAt(i,j, 2);
}
