// ProjetFourmi.cpp : This file contains the 'main' function. Program execution begins and ends there.


#include "general.hpp"
#include "mersenneTwister.hpp"

#include "manager.hpp"

#include "caseMap.hpp"
#include "fourmilliere.hpp"
#include "fourmi.hpp"
#include "pheromone.hpp"

/*!
 * \file projetFourmi.cpp
 * \brief Liste des variables globales constantes
 * @see Fourmi::rechargerPointsActionDebutTour()
 * @see Fourmi::trophallaxie(int)
 * @see Fourmi::ramasserBouffe()
 * @see Fourmi::changerDeCase(Direction)
 * @see Fourmi::debutActionReconnaissanceEtPriseDeDecision()
 * @see Fourmi::entrerDansFourmilliere()
 * @see Fourmi::sortirDeFourmilliere()
 */
#include <chrono>
#include <thread>

int main()
{
   bool toutVaBien = true;

   Manager manager;
   manager.initialiserMap();
   manager.initialiserListeFourmillieres();
   
   int tour = 0;
   while(toutVaBien && tour < NBRMAXTOURS)
   {

      using namespace std::this_thread;
      using namespace std::chrono;
      sleep_for(seconds(1));
      tour++;

      cout << "\n Tour " << tour << " sur " << NBRMAXTOURS << "\n";
      manager.actionsDeDebutDeTour();
      manager.afficherMapHexa();


      list<int> listeIndicesFourmisNonJoued;
      for(int fourmi = 0; fourmi < NBRFOURMILLIERES * NBRMAXFOURMISPARFOURMILLIERE; fourmi++)
      {
         int indiceFourmilliere = fourmi / NBRMAXFOURMISPARFOURMILLIERE;
         int indiceFourmi = fourmi % NBRMAXFOURMISPARFOURMILLIERE;

         if(manager.GetListeFourmilliere()[indiceFourmilliere]->GetListeFourmisLoyales()[indiceFourmi] != nullptr) // si cette fourmi existe
         {
            listeIndicesFourmisNonJoued.push_back(fourmi); // , alors on note qu'elle doit jouer son tour
         }
      }

      cout << "Nombre fourmis vivantes : " << listeIndicesFourmisNonJoued.size() << "\n";

      while(toutVaBien && listeIndicesFourmisNonJoued.size() > 0) // toujours remplacement random biaisé
      {

         list<int>::iterator iterateurListeIndicesFourmisNonJoued = listeIndicesFourmisNonJoued.begin();

         int tirage = uniform_int(0, listeIndicesFourmisNonJoued.size() - 1);
         for(int i = 0; i < tirage; i++)
         {
               iterateurListeIndicesFourmisNonJoued++;
         }
         int indiceFourmilliereChoisie = *iterateurListeIndicesFourmisNonJoued / NBRMAXFOURMISPARFOURMILLIERE;
         int indiceFourmiChoisie = *iterateurListeIndicesFourmisNonJoued % NBRMAXFOURMISPARFOURMILLIERE;


         //Fais jouer le tour
         Fourmi* fourmiPlaying = manager.GetListeFourmilliere()[indiceFourmilliereChoisie]->GetListeFourmisLoyales()[indiceFourmiChoisie];

         if(fourmiPlaying != nullptr)
         {
            bool survieFourmi = fourmiPlaying->survieFourmi();

            fourmiPlaying->rechargerPointsActionDebutTour();

            while(toutVaBien && survieFourmi && fourmiPlaying->GetPointsActionfourmi() > 0)
            {
               if(fourmiPlaying->toutEstOkDebutAction())
               {
                  if(AFFICHERINFOSDEVELOPPEUR)
                  {
                     cout << "debut de l'action --> food : " << fourmiPlaying->GetJabotSocialLevel() << "; carry : " << fourmiPlaying->GetCarryLevel() << "; nbr de PA : " << fourmiPlaying->GetPointsActionfourmi() << "; position sur case : " << fourmiPlaying->GetPositionFourmiSurCase() << "; etat fourmi : " << fourmiPlaying->GetEtatFourmi() << "\n";
                  }
                  

                  DecisionFourmi decisionPrise = fourmiPlaying->debutActionReconnaissanceEtPriseDeDecision();

                  if(!AFFICHERINFOSDEVELOPPEUR)
                  {
                     //RÉALISATION DE LA DECISION
                     if(decisionPrise == RIEN)
                     {
                        fourmiPlaying->resterInactiveDansFourmilliere();
                     }
                     else if(decisionPrise == ENTRERDANSFOURMILLIERE)
                     {
                        fourmiPlaying->entrerDansFourmilliere();
                     }
                     else if(decisionPrise == SORTIRDEFOURMILLIERE)
                     {
                        fourmiPlaying->sortirDeFourmilliere();
                     }
                     else if(decisionPrise == RAMASSERDELABOUFFE)
                     {
                        fourmiPlaying->ramasserBouffe();
                     }
                     else if(decisionPrise == ALLERHAUTGAUCHE)
                     {
                        fourmiPlaying->changerDeCase(HAUTGAUCHE);
                     }
                     else if(decisionPrise == ALLERHAUTDROIT)
                     {
                        fourmiPlaying->changerDeCase(HAUTDROIT);
                     }
                     else if(decisionPrise == ALLERMILIEUDROITE)
                     {
                        fourmiPlaying->changerDeCase(MILIEUDROITE);
                     }
                     else if(decisionPrise == ALLERBASDROITE)
                     {
                        fourmiPlaying->changerDeCase(BASDROITE);
                     }
                     else if(decisionPrise == ALLERBASGAUCHE)
                     {
                        fourmiPlaying->changerDeCase(BASGAUCHE);
                     }
                     else if(decisionPrise == ALLERMILIEUGAUCHE)
                     {
                        fourmiPlaying->changerDeCase(MILIEUGAUCHE);
                     }
                     else if(decisionPrise == ERREURDECISIONELLE)
                     {
                        toutVaBien = false;
                        cout << "Erreur décisionnelle !! \n";
                     }
                     else
                     {
                        toutVaBien = false;
                        cout << "la décision prise n'existe pas !! \n";
                     }
                  }
                  else
                  {  
                     //RÉALISATION DE LA DECISION
                     if(decisionPrise == RIEN)
                     {
                        cout << "J'ai pris la décision de ne rien faire \n";
                        fourmiPlaying->resterInactiveDansFourmilliere();
                     }
                     else if(decisionPrise == ENTRERDANSFOURMILLIERE)
                     {
                        cout << "J'ai pris la décision rentrer dans la fourmilliere \n";
                        fourmiPlaying->entrerDansFourmilliere();
                     }
                     else if(decisionPrise == SORTIRDEFOURMILLIERE)
                     {
                        cout << "J'ai pris la décision sortir de la fourmilliere \n";
                        fourmiPlaying->sortirDeFourmilliere();
                     }
                     else if(decisionPrise == RAMASSERDELABOUFFE)
                     {
                        cout << "J'ai pris la décision de ramasser de la bouffe \n";
                        fourmiPlaying->ramasserBouffe();
                     }
                     else if(decisionPrise == ALLERHAUTGAUCHE)
                     {
                        cout << "J'ai pris la décision haut gauche \n";
                        fourmiPlaying->changerDeCase(HAUTGAUCHE);
                     }
                     else if(decisionPrise == ALLERHAUTDROIT)
                     {
                        cout << "J'ai pris la décision haut droit \n";
                        fourmiPlaying->changerDeCase(HAUTDROIT);
                     }
                     else if(decisionPrise == ALLERMILIEUDROITE)
                     {
                        cout << "J'ai pris la décision milieu droite \n";
                        fourmiPlaying->changerDeCase(MILIEUDROITE);
                     }
                     else if(decisionPrise == ALLERBASDROITE)
                     {
                        cout << "J'ai pris la décision bas droite \n";
                        fourmiPlaying->changerDeCase(BASDROITE);
                     }
                     else if(decisionPrise == ALLERBASGAUCHE)
                     {
                        cout << "J'ai pris la décision bas gauche \n";
                        fourmiPlaying->changerDeCase(BASGAUCHE);
                     }
                     else if(decisionPrise == ALLERMILIEUGAUCHE)
                     {
                        cout << "J'ai pris la décision milieu gauche \n";
                        fourmiPlaying->changerDeCase(MILIEUGAUCHE);
                     }
                     else if(decisionPrise == ERREURDECISIONELLE)
                     {
                        toutVaBien = false;
                        cout << "Erreur décisionnelle !! \n";
                     }
                     else
                     {
                        toutVaBien = false;
                        cout << "la décision prise n'existe pas !! \n";
                     }

                     cout << "fin de l'action --> food : " << fourmiPlaying->GetJabotSocialLevel() << "; carry : " << fourmiPlaying->GetCarryLevel() << "; nbr de PA : " << fourmiPlaying->GetPointsActionfourmi() << "; position sur case : " << fourmiPlaying->GetPositionFourmiSurCase() << "; etat fourmi : " << fourmiPlaying->GetEtatFourmi() << "\n"; 

                  }
               }
               else
               {
                  cout << "Probleme au début du tour de la fourmi\n";
                  toutVaBien = false;
               }
            }


            if(survieFourmi) // Si la fourmi a survécu à son tour, alors j'incrémente l'âge des fourmis.
            {
               fourmiPlaying->vieillirFourmi();
            }
            else
            {
               if(AFFICHERINFOSDEVELOPPEUR)
               {
                  cout << "decesFourmi\n";
               }
               manager.GetListeFourmilliere()[indiceFourmilliereChoisie]->decesFourmi(fourmiPlaying); // s'occupe de transformer le cadavre en bouffe à l'endroit où la fourmi est morte (fourmilliere OU case)
            }
         }
         else
         {
            cout << "fourmiPlaying est un nullptr !! \n";
            toutVaBien = false;
         }

         


         listeIndicesFourmisNonJoued.erase(iterateurListeIndicesFourmisNonJoued); // remplacement random pas ouf
         if(AFFICHERINFOSDEVELOPPEUR)
         {
            cout << listeIndicesFourmisNonJoued.size() << "\n";
         }
      }


      cout << "\n Fin du tour " << tour << " sur " << NBRMAXTOURS << "\n";
   }

   if(toutVaBien)
   {
      cout << "\n\nLes " << NBRMAXTOURS << " tours ont été joués, fin standard de la simulation\n\n";
      int temp = 0;
      for(int i = 0; i < NBRFOURMILLIERES; i++)
      {
      //   cout << "Compte-rendu actions de la fourmilliere " << i << " :\n";
      //   cout << manager.GetListeFourmilliere()[i]->CRactionsFourmilliere << "\n_______________________\n";
      temp += manager.GetListeFourmilliere()[i]->totalcollectetfood;
      }
      cout << "total collected food " << temp << "\n";
   }
   else
   {
      cout << "\n\nErreur survenue, fin forcée de la simulation\n\n";
   }


   return 0;
}
