#include "caseMap.hpp"


CaseMap::CaseMap(int iNouvellecase, int jNouvelleCase, PaysageCase paysageNouvelleCase, int bouffeNouvelleCase)
{
    iCase = iNouvellecase;
    jCase = jNouvelleCase;

    paysageCase = paysageNouvelleCase;
    bouffeQuantity = bouffeNouvelleCase;

    fourmilliereCase = nullptr;

    nbrFourmisPresentes = 0;
    for(int indiceFourmi = 0; indiceFourmi < NBRMAXFOURMISPARCASE; indiceFourmi++)
    {
        listeFourmisPresentes[indiceFourmi] = nullptr;
    }

    listeCasesVoisines = new CaseMap*[NBRCASESVOISINES];
    for(int direction = HAUTGAUCHE; direction != MILIEUGAUCHE; direction++)
    {
        listeCasesVoisines[direction] = nullptr;
    }

    //cout << "Je créée une case \n";
}


CaseMap::~CaseMap()
{
    if(listeCasesVoisines != nullptr)
    {
        delete [] listeCasesVoisines;
    }
}

CaseMap* CaseMap::GetCaseVoisine(int directionCaseVoisine){return listeCasesVoisines[directionCaseVoisine];}

void CaseMap::SetCaseVoisine(CaseMap* nouvelleCaseVoisine, int directionNouvelleCaseVoisine){listeCasesVoisines[directionNouvelleCaseVoisine] = nouvelleCaseVoisine;}



//____________________________________________________________________________________________________



void CaseMap::ajouterPheromone(Pheromone nouveauPheromone, Direction directionNouveauPheromone)
{
    listePheromoneCase[directionNouveauPheromone].push_front(nouveauPheromone);
}

void CaseMap::faireVieillirPheromones()
{
    for(int direction = 0; direction < NBRCASESVOISINES; direction++)
    {
        for(list<Pheromone> :: iterator itPheromone = listePheromoneCase[direction].begin(); itPheromone != listePheromoneCase[direction].end(); itPheromone++)
        {
            (*itPheromone).faireVieillirPheromone();
        }
        while(listePheromoneCase[direction].back().GetAgePheromone() > AGEMAXPHEROMONE)
        {
            listePheromoneCase[direction].pop_back();
        }
    }
}


/***************************************************************************//**
 * appel uniquement depuis bool Fourmi::toutEstOkDebutAction() donc on part du principe que les tests de cette fonction-là ont été validés
 ******************************************************************************/
bool CaseMap::toutEstOkSurCaseDebutAction() // appel uniquement depuis bool Fourmi::toutEstOkDebutAction() donc on part du principe que les tests de cette fonction-là ont été validés
{
    bool toutEstOkSurCase = false;

    if(bouffeQuantity >= 0 && nbrFourmisPresentes >= 0 && nbrFourmisPresentes <= NBRMAXFOURMISPARCASE) // Verification parametres basiques de la case concernée                
    {
        bool pasDeProbleme = true;
        int compteurFourmisPresentes = 0;

        int indiceFourmiPresente = 0;
        while(pasDeProbleme && indiceFourmiPresente < NBRMAXFOURMISPARCASE)
        {
            if(listeFourmisPresentes[indiceFourmiPresente] != nullptr)
            {
                pasDeProbleme = false;
                if(listeFourmisPresentes[indiceFourmiPresente]->GetCaseActuelle() == this)
                {
                    if(listeFourmisPresentes[indiceFourmiPresente]->GetPositionFourmiSurCase() != -1)
                    {
                        if(listeFourmisPresentes[indiceFourmiPresente]->GetPositionFourmiSurCase() == indiceFourmiPresente)
                        {
                            compteurFourmisPresentes++;
                            pasDeProbleme =true;
                        }
                        else
                        {
                            cout << "D'apres la case, la fourmi pense etre à une certaine position alors qu'elle n'y est pas\n";
                        }
                    }
                    else
                    {
                        cout << "une fourmi censée etre présente sur la case pense etre dans une fourmilliere!! \n";
                    }
                }
                else
                {
                    cout << "Une fourmi censée etre présente sur une case est en fait sur une autre case!! \n";
                }
            }

            indiceFourmiPresente++;
        }

        if(pasDeProbleme && compteurFourmisPresentes == nbrFourmisPresentes)
        {
            toutEstOkSurCase = true;
        }
        else
        {
            cout << "Probleme pendant le comptage ou alors le nbr de fourmis présentes annoncé ne correspond pas\n";
        }
    }
    else
    {
        cout << "bouffeQuantity case :" << bouffeQuantity << "; nombre fourmis présentes : " << nbrFourmisPresentes << "\n";
        cout << "Probleme dans les parametres basiques de la case (bouffeQuantity négative ou nbrFourmisPresentes bizarre)\n";
    }
    

    return toutEstOkSurCase;
}



int CaseMap::ajouterFourmi(Fourmi* fourmiArrivante)
{
    int indiceAjoutFourmi = -1;

    if(nbrFourmisPresentes < NBRMAXFOURMISPARCASE)
    {
        if(fourmiArrivante != nullptr)
        {
            int iSlot = 0;

            while(indiceAjoutFourmi == -1 && iSlot < NBRMAXFOURMISPARCASE)
            {
                if(listeFourmisPresentes[iSlot] == nullptr)
                {
                    listeFourmisPresentes[iSlot] = fourmiArrivante;
                    nbrFourmisPresentes++;

                    if(AFFICHERINFOSDEVELOPPEUR)
                    {
                        cout << "fourmi ajoutée\n";
                    }
                    indiceAjoutFourmi = iSlot;
                }

                iSlot++;
            }

            if(indiceAjoutFourmi == -1)
            {
                cout << "pas trouvé de slot disponible pour ajouter la fourmi à la case alors que théoriquement y a assez de place\n";
            }
        }
        else
        {
            cout << "Probleme avec la fourmi arrivante, c'est un pointeur nul :/\n";
        }
    }
    else
    {
        cout << "La case est déjà pleine, on ne peut pas ajouter de fourmi\n";
    }

    return indiceAjoutFourmi;
}


bool CaseMap::retirerFourmi(Fourmi* fourmiPartante)
{
    bool fourmiPartie = false;

    if(fourmiPartante != nullptr)
    {
        if(fourmiPartante->GetPositionFourmiSurCase() != -1)
        {
            listeFourmisPresentes[fourmiPartante->GetPositionFourmiSurCase()] = nullptr;
            nbrFourmisPresentes--;

            if(AFFICHERINFOSDEVELOPPEUR)
            {
                cout << "fourmi retiree\n";
            }
            fourmiPartie = true;
        }
        else
        {
            cout << "La fourmi essaie de partir de sa case alors qu'elle est toujours dans la fourmilliere de la case\n";
        }
    }
    else
    {
        cout << "Probleme avec la fourmi partante, c'est un pointeur nul :/\n";
    }


    return fourmiPartie;
}



//____________________________________________________________________________________________________

Fourmilliere* CaseMap::GetFourmilliereCase(){return fourmilliereCase;}
void CaseMap::SetFourmilliereCase(Fourmilliere* nouveauFourmilliereCase){fourmilliereCase = nouveauFourmilliereCase;}

int CaseMap::GetNbrFourmisPresentes(){return nbrFourmisPresentes;}

Fourmi** CaseMap::GetListeFourmisPresentes(){return listeFourmisPresentes;}


//____________________________________________________________________________________________________



int CaseMap::GetNbrPheromones(){return listePheromoneCase[0].size() + listePheromoneCase[1].size() + listePheromoneCase[2].size() + listePheromoneCase[3].size() + listePheromoneCase[4].size() + listePheromoneCase[5].size();}

/***************************************************************************//**
 * renvoi le nombre de phéromone du type recherché sur cette case
 * @param TypePheromone
 ******************************************************************************/
int CaseMap::GetNbrPheromones(TypePheromone typeRecherched)
{
    int nbrPheromonesDeCeType = 0;

    for(int direction = 0; direction < NBRCASESVOISINES; direction++)
    {
        nbrPheromonesDeCeType += GetNbrPheromones(typeRecherched, (Direction)direction);
    }


    return nbrPheromonesDeCeType;
}





int CaseMap::GetNbrPheromones(TypePheromone typeRecherched, Direction directionRecherched)
{
    int nbrPheromonesDeCeTypeEtDeCetteDirection = 0;

    for(list<Pheromone>::iterator it = listePheromoneCase[directionRecherched].begin(); it != listePheromoneCase[directionRecherched].end(); it++)
    {
        if((*it).GetTypePheromone() == typeRecherched)
        {
            nbrPheromonesDeCeTypeEtDeCetteDirection++;
        }
    }


    return nbrPheromonesDeCeTypeEtDeCetteDirection;
}


/***************************************************************************//**
 * renvoi le nombre de phéromone du type recherché sur cette case qui appartiennent a la fourmillière
 * @param TypePheromone
 * @param int
 ******************************************************************************/
int CaseMap::GetNbrPheromonesAmis(TypePheromone typeRecherched, int indiceFourmilliere)
{
    int nbrPheromonesDeCeTypeEtDeCetteFourmilliere = 0;

    for(int direction = 0; direction < NBRCASESVOISINES; direction++)
    {
        nbrPheromonesDeCeTypeEtDeCetteFourmilliere += GetNbrPheromonesAmis(typeRecherched, (Direction)direction, indiceFourmilliere);
    }


    return nbrPheromonesDeCeTypeEtDeCetteFourmilliere;
}


int CaseMap::GetNbrPheromonesAmis(TypePheromone typeRecherched, Direction directionRecherched, int indiceFourmilliere)
{
    int nbrPheromonesDeCeTypeDeCetteDirectionEtDeCetteFourmilliere = 0;

    for(list<Pheromone>::iterator it = listePheromoneCase[directionRecherched].begin(); it != listePheromoneCase[directionRecherched].end(); it++)
    {
        if((*it).GetTypePheromone() == typeRecherched && (*it).GetSignatureFourmillierePheromone() == indiceFourmilliere)
        {
            nbrPheromonesDeCeTypeDeCetteDirectionEtDeCetteFourmilliere++;
        }
    }


    return nbrPheromonesDeCeTypeDeCetteDirectionEtDeCetteFourmilliere;
}


PaysageCase CaseMap::GetPaysageCase(){return paysageCase;}
//void CaseMap::SetPaysageCase(PaysageCase nouveauPaysageCase){paysageCase = nouveauPaysageCase;}
int CaseMap::GetBouffeQuantity(){return bouffeQuantity;}
void CaseMap::SetBouffeQuantity(int nouveauBouffeQuantity){bouffeQuantity = nouveauBouffeQuantity;}

