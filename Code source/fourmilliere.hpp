#ifndef FOURMILLIERE_HPP
#define FOURMILLIERE_HPP

#include "general.hpp"
#include "mersenneTwister.hpp"

#include "fourmi.hpp"
#include "caseMap.hpp"

class Fourmi;
class CaseMap;

class Fourmilliere
{
    private:
        int indiceFourmilliere;
        CaseMap * caseMapFourmilliere;

        int niveauBouffeDansFourmilliere;
        bool reineVivante;

        int nbrFourmisLoyales;
        Fourmi** listeFourmisLoyales;

        int nbrFourmisPresentes;
        bool* listeFourmisPresentes;


    public:
        int totalcollectetfood = 0;

        Fourmilliere(int, CaseMap*);
        ~Fourmilliere();

        string CRactionsFourmilliere;

        bool toutEstOkDansFourmilliereDebutAction(); // vérifie notamment si toutes les fourmis présentes sont bel et bien à l'endroit qu'elles pensent

        //void depotBouffe(int);
        void playTurnFourmilliere(); // représente le tour joué par une fourmillière : nourrir la reine et déterminer combien de fourmis vont être pondues. False : la reine n'a pas assez reçu à manger, elle meurt et donc la fourmilliere meurt. 

        bool fourmiEstLoyale(Fourmi*);
        bool fourmiEstLoyaleEtPresente(Fourmi*);

        void nourrirFourmiInactive();

        void pondreFourmi();
         void decesFourmi(Fourmi*);

        bool entreeFourmi(Fourmi*); // Une fourmi entrante dépose automatiquement toute la bouffe qu'elle a
        bool sortieFourmi(Fourmi*);

        EtatFourmilliere GetEtatFourmilliere();
        int quantityProvisionsAutorisedPourSortie();


        //Getters
        int GetIndiceFourmilliere();
        int GetNiveauBouffe();
        int GetNbrfourmisLoyales();
        Fourmi** GetListeFourmisLoyales();
        int GetNbrfourmisPresentes();
        bool* GetListeFourmisPresentes();
        
};



#endif
