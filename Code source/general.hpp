#ifndef GENERAL_HPP
#define GENERAL_HPP

#include <iostream>
#include <list>
#include <ctime>
#include <cstdlib>
#include <cmath>

using namespace std;

/*!
 * \file general.hpp
 * \brief Liste des variables globales constantes
 */

/* Period parameters pour Mersenne twister */  
const int N = 624;
const int M = 397;
const long unsigned int MATRIX_A = 0x9908b0dfUL;   /* constant vector a */
const int UPPER_MASK = 0x80000000UL; /* most significant w-r bits */
const int LOWER_MASK = 0x7fffffffUL; /* least significant r bits */



const bool AFFICHERINFOSDEVELOPPEUR = false;
const int NBRMAXTOURS = 120;

const int LONGUEURMAP = 25;
const int LARGEURMAP = 25;

const int NBRTYPESPHEROMONES = 3;
const int NBRCASESVOISINES = 6;
const int AGEMAXPHEROMONE = 13; //181 485 //vie 100 tour // vie 50 tour 168 000

const int STARTERPACKDEBOUFFEFOURMILLIERE = 10000;
const int QUANTITYBOUFFERECHARGEMENTMAP = 00;
const int MINIMUMDECASESRECHARGED = 5;

const int NBRFOURMILLIERES = 6;

const int NBRMAXFOURMISPARFOURMILLIERE = 500;
const int NBRMAXFOURMISPARCASE = 20;

const int NBRPAFOURMI = 5;
const int BONUSNBRPAFOURMIDOPED = 5;
const int MINIMUMPAOCTROYED = 1;

const int RECUPJAUGEDEFATIGUE = 20;
const int PALLIERMALUSFATIGUE = 10;

const double PROBACHERCHERBOUFFEQUANDFATIGUATEDETINCROYABLEGOPONDRE = 0.1;
const double PROBACHERCHERBOUFFEQUANDFATIGUATEDETTOUTVABIEN = 0.3;
const double PROBACHERCHERBOUFFEQUANDFATIGUATEDETFAMINE = 0.6;

const float MINJABOTSOCIALLEVELTROPHALLAXIEDONNANTE = 0.4;
const float MAXJABOTSOCIALLEVELTROPHALLAXIERECEVANTE = 0.10;
const float COEFNIVEAUJABOTSOCIALDEMITOUR = 0.5;

const float COEFREDUCTIONDELTAJABOTSOCIALAPRESTROPHALLAXIE = 0.5;
const int MAXCARRYLEVELFOURMI = 2000;
const int MAXJABOTSOCIALLEVELFOURMI = 200;
const int AGEMAXFOURMIS = 100; // Nombre de tours max que la fourmi peut jouer avant de mourir 
const float PROBASURVIE = powf(0.5, 1.0/(AGEMAXFOURMIS * NBRPAFOURMI)); // 50% de chances survivre avant d'atteindre l'âge maximum où elle meurt instantanément. On considère qu'elle fait NBRPAFOURMI actions par tour
const double COEFDURABILITEPROPAGATIONALERTE = 0.04; // entre 0 et + infini. Avec 0.04 elle a 45% de chances de transmettre l'alerte avec nbrActionsDepuisAlerte = 20. Si 0, alors 100% de chances de transmettre l'alerte, quelque soit nbrActionsDepuisAlerte. Si trop grand, alors elle aura très peu de chances de transmettre l'alerte. 


const int BOUFFEPOURNOURRIRREINE = 50;
const int BOUFFEPOURPONDREFOURMI = 100;

const int NIVEAUBOUFFEPARFOURMILOYALEPONTE = MAXJABOTSOCIALLEVELFOURMI;
const int NIVEAUBOUFFEPARFOURMILOYALEFAMINE = 50;
const int NBRTOURSSURVIEULTIMEREINE = 5; // si le niveau de bouffe de la fourmilliere est inférieur ou égal à TOURSSURVIEULTIMEREINE * NOURRIRREINE, alors toutes les fourmis présentes ne mangent plus et partent en exploration sans provisions et la reine n'a plus qu'à espérer que les fourmis déjà parties en exploration ramènent de la bouffe.

const int NBRTOURSPROVISIONSEXPLORATIONFAMINE = 10; // On commence à rationner quand c'est la famine
const int NBRTOURSPROVISIONSSURVIEULTIME = 3;

/** Represente le type de terrain et leur difficulté */
enum PaysageCase
{
    PLAT = 1,
    HERBE = 2,
    ROCHER = 4,
    EAU = 8,
    IMPRATICABLE = -1
};

enum TypePheromone
{
    ALERTE,
    BOUFFE,
    FOURMILLIERE
};

enum DecisionFourmi
{
    ERREURDECISIONELLE,
    RIEN,
    ENTRERDANSFOURMILLIERE,
    SORTIRDEFOURMILLIERE,
    RAMASSERDELABOUFFE,
    ALLERHAUTGAUCHE,
    ALLERHAUTDROIT,
    ALLERMILIEUDROITE,
    ALLERBASDROITE,
    ALLERBASGAUCHE,
    ALLERMILIEUGAUCHE
};

enum CoutEnBouffeDecisionFourmi
{
    RESTERINACTIVE = 1,
    ENTREEOUSORTIEFOURMILLIERE = 1,
    RAMASSERBOUFFE = 2,
    BOUGERDE1PA = 3
};

enum Direction
{
    HAUTGAUCHE,
    HAUTDROIT,
    MILIEUDROITE,
    BASDROITE,
    BASGAUCHE,
    MILIEUGAUCHE
};

enum EtatFourmilliere
{
    INCROYABLEGOPONDRE,
    TOUTVABIEN ,
    FAMINE,
    URGENCEFAMINE,
    SURVIEULTIME,
    REINEMORTE
};

    /** 
     * Enum des état possibles d'une Fourmi
     */
enum EtatFourmi // des phéromones ne vont pas provoquer le changement de l'état d'une fourmi
{
    ATTENDRE, /**< La Fourmi ne peut pas ou ne veut pas agir. */
    CHERCHERBOUFFE,/**< La Fourmi parcours la carte a la recherche de nourriture. */
    RAMENERBOUFFE,/**< La Fourmi a trouver de la nourriture et la ramène a la Fourmilliere. */
    ALERTED,/**< La Fourmi a rencontrer une Fourmi d'une autre Fourmilliere et rentre. */
    ZOMBIE/**< Fourmi dont la Fourmilliere est morte. */
};

#endif