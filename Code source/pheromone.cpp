#include "pheromone.hpp"


Pheromone::Pheromone(TypePheromone typeNouvellePheromone, int signatureFourmilliereNouvellePheromone)
{
    //cout << "je créée un nouveau phéromone\n";

    typePheromone = typeNouvellePheromone;
    agePheromone = 0;
    signatureFourmillierePheromone = signatureFourmilliereNouvellePheromone;
}

TypePheromone Pheromone::GetTypePheromone(){return typePheromone;}
int Pheromone::GetAgePheromone(){return agePheromone;}
void Pheromone::faireVieillirPheromone(){agePheromone++;}
int Pheromone::GetSignatureFourmillierePheromone(){return signatureFourmillierePheromone;}


