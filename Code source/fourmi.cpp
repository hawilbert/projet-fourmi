#include "fourmi.hpp"

Fourmi::Fourmi(int NouveauIndiceListeFourmisLoyalesFourmilliere, int indiceFourmilliereNouvelleFourmi, CaseMap* caseMapNouvelleFourmi)
{
    indiceListeFourmisLoyalesFourmilliere = NouveauIndiceListeFourmisLoyalesFourmilliere;
    indiceFourmilliere = indiceFourmilliereNouvelleFourmi;
    positionFourmiSurCase = -1;

    carrylevel = 0;
    jabotSocialLevel = 0;
    ageFourmi = 0;

    etatFourmi = ATTENDRE;
    nbrActionsDepuisAlerte = 0;
    etatFourmilliere = INCROYABLEGOPONDRE;
    jaugeDeFatigue = 0;
    pointsAction = 0;

    caseActuelle = caseMapNouvelleFourmi;
    //cout << "Je créée une fourmi \n";
}


Fourmi::~Fourmi()
{
    //cout << "Je détruis une fourmi\n";
}

/***************************************************************************//**
 * Retourne la position dans la liste des Fourmis loyales de la Fourmilliere mère.
 * @see Fourmilliere
 ******************************************************************************/
int Fourmi::GetIndiceListeFourmisLoyalesFourmilliere(){return indiceListeFourmisLoyalesFourmilliere;}

/***************************************************************************//**
 * Retourne l'indice de la Fourmilliere dans la liste du Manager.
 * @see Manager
 ******************************************************************************/
int Fourmi::GetIndiceFourmilliere(){return indiceFourmilliere;}

/***************************************************************************//**
 * Retourne la position de la Fourmi dans la liste de la case ou elle est.
 * @see CaseMap
 ******************************************************************************/
int Fourmi::GetPositionFourmiSurCase(){return positionFourmiSurCase;}

/***************************************************************************//**
 * Retourne la case ou elle est.
 * @see CaseMap
 ******************************************************************************/
CaseMap* Fourmi::GetCaseActuelle(){return caseActuelle;}

/***************************************************************************//**
 * Renvoie l'age (en nombre de tour) de la Fourmi depuis sa ponte.
 ******************************************************************************/
int Fourmi::GetAgeFourmi(){return ageFourmi;}

/***************************************************************************//**
 * Renvoie l'état de la Fourmi
 * 
 * @see EtatFourmi
 ******************************************************************************/
EtatFourmi Fourmi::GetEtatFourmi(){return etatFourmi;}

/***************************************************************************//**
 * Renvoie le niveau de nourriture dans le jabot de la Fourmi. C'est nourriture sert a l'exploration ou a l'échange avec d'autre Fourmi.
 * @see toutEstOkDebutAction()
 * @see trophallaxie(int)
 ******************************************************************************/
int Fourmi::GetJabotSocialLevel(){return jabotSocialLevel;}
void Fourmi::SetJabotSocialLevel(int nouveauFoodLevel){jabotSocialLevel = nouveauFoodLevel;}

/***************************************************************************//**
 * Renvoi le niveau de nourriture que la Fourmi transporte pour ramener à la Fourmilliere.
 * @see toutEstOkDebutAction()
 * @see ramasserBouffe()
 ******************************************************************************/
int Fourmi::GetCarryLevel(){return carrylevel;}
void Fourmi::SetCarryLevel(int nouveauCarryLevel){carrylevel = nouveauCarryLevel;}

/***************************************************************************//**
 * Nombre de point d'action restant de la Fourmi. Ces points d'action sont utilisé pour toutes les actions effecutées par la Fourmi. Chaque deplacement ou trophallaxie(int) consomme des points. Ces points peuvent être négatifs. Elle regagne des points à chaque tour et agit tant qu'il lui en reste et qu'elle peu agir. Si elle commence son tour avec un nombre de point négatif, elle n'agit pas.
 * @see toutEstOkDebutAction()
 * @see rechargerPointsActionDebutTour()
 ******************************************************************************/
int Fourmi::GetPointsActionfourmi(){return pointsAction;}

/***************************************************************************//**
 * Renvoie le nombre de tour de puis la dernière alerte.
 * @see SetEtatFourmiALERTED()
 ******************************************************************************/
int Fourmi::GetNbrActionsDepuisAlerte(){return nbrActionsDepuisAlerte;}
void Fourmi::SetNbrActionsDepuisalerte(int nouveauNbrActionsDepuisAlerte){nbrActionsDepuisAlerte = nouveauNbrActionsDepuisAlerte;}


// si fourmi est dans une fourmilliere, alors on vérifie si tout va bien dans la fourmilliere et dans la case de la fourmilliere.
// si fourmi est sur une case sans fourmilliere, alors on vérifie si tout va bien dans la case et dans les cases voisines.
// si fourmi est sur une case avec fourmilliere, alors on vérifie si tout va bien dans la fourmilliere, la case et les cases voisines.
/***************************************************************************//**
 * Verifie un nombre de paramètre avant de laisser la fourmi agir dans main() : 
 * - si fourmi est dans une Fourmilliere, alors on vérifie si tout va bien dans la Fourmilliere et dans la case de la Fourmilliere.
 * - si fourmi est sur une case sans Fourmilliere, alors on vérifie si tout va bien dans la case et dans les cases voisines.
 * - si fourmi est sur une case avec Fourmilliere, alors on vérifie si tout va bien dans la Fourmilliere, la case et les cases voisines.
 * 
 * Plus précisément, voici la liste possible d'erreurs vérifiées : 
 - La fourmi est dans la Fourmilliere en début d'action alors que son etat n'est pas ATTENDRE.
 - La fourmi pense etre présente dans une Fourmilliere alors qu'elle ne l'est pas d'apres la Fourmilliere.
 - La fourmi serait dans une Fourmilliere dont elle n'est pas loyale d'apres la Fourmilliere.
 - La fourmi pense etre dans une Fourmilliere amie alors qu'elle ne l'ai pas.
 - Il y a un problème dans la Fourmilliere qu'il y a sur la case.
 - La fourmi pense etre dans Fourmilliere sauf que il n'y a pas de Fourmilliere sur sa case.
 - Il y a un problème sur au moins une des cases voisines de la fourmi.
 - Il y a un problème sur la case de la fourmi.
 - La fourmi est sur une case IMPRATICABLE ou alors la fourmi est dans une Fourmilliere qui est elle-même sur une case IMPRATICABLE.
 - La case actuelle de la fourmi est nullptr.
 - Il y a un probleme sur les indices listesloyales, Fourmilliere ou positionSurCase.
 - Il y a un probleme sur la jauge de fatigue, age de la fourmi ou nbrActionsDepuisAlerte.
 - il y a un probleme sur les jabot social levels ou carry level de la fourmi.

 *
 * 
 *  
 * @see projetFourmi.cpp
 ******************************************************************************/
bool Fourmi::toutEstOkDebutAction() 
{
    bool toutEstOk = false;

    if(carrylevel >= 0 && carrylevel <= MAXCARRYLEVELFOURMI && /*jabotSocialLevel >= 0 &&*/ jabotSocialLevel <= MAXJABOTSOCIALLEVELFOURMI) // verification des parametres basiques d'une fourmi
    {
        if(/*jaugeDeFatigue >= 0 &&*/ ageFourmi >= 0 && ageFourmi <= AGEMAXFOURMIS && nbrActionsDepuisAlerte >= 0) // verification des parametres basiques d'une fourmi
        {
            if(indiceListeFourmisLoyalesFourmilliere >= 0 && indiceListeFourmisLoyalesFourmilliere < NBRMAXFOURMISPARFOURMILLIERE && // verification des parametres basiques d'une fourmi
                indiceFourmilliere >= 0 && indiceFourmilliere < NBRFOURMILLIERES && positionFourmiSurCase >= -1 && positionFourmiSurCase < NBRMAXFOURMISPARCASE) 
                {
                    if(caseActuelle != nullptr)
                    {
                        if(caseActuelle->GetPaysageCase() != IMPRATICABLE)
                        {
                            if(caseActuelle->toutEstOkSurCaseDebutAction())
                            {
                                bool explorerCasesVoisines = false;
                                if(caseActuelle->GetFourmilliereCase() != nullptr) // si il y a une fourmilliere sur la case
                                {
                                    if(caseActuelle->GetFourmilliereCase()->toutEstOkDansFourmilliereDebutAction())
                                    {
                                        if(positionFourmiSurCase == -1) // si la fourmi pense etre dans une fourmilliere
                                        {
                                            if(caseActuelle->GetFourmilliereCase()->GetIndiceFourmilliere() == indiceFourmilliere)
                                            {
                                                if(caseActuelle->GetFourmilliereCase()->fourmiEstLoyale(this))
                                                {
                                                    if(caseActuelle->GetFourmilliereCase()->fourmiEstLoyaleEtPresente(this))
                                                    {
                                                        if(etatFourmi == ATTENDRE)
                                                        {
                                                            toutEstOk = true;
                                                        }
                                                        else
                                                        {
                                                            cout << "La fourmi est dans la fourmilliere en début d'action alors que son etat n'est pas ATTENDRE\n";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cout << "apparemment la fourmi pense etre présente dans une fourmilliere alors qu'elle ne l'est pas d'apres la fourmilliere\n";
                                                    }
                                                }
                                                else
                                                {
                                                    cout << "apparemment la fourmi serait dans une fourmilliere dont elle n'est pas loyale d'apres la fourmilliere mdr\n";
                                                }
                                            }
                                            else
                                            {
                                                cout << "la fourmi pense etre dans une fourmilliere amie alors que non en fait lol (pas le meme indiceFourmilliere)\n";
                                            }
                                        }
                                        else // Si la fourmi ne pense pas etre dans une fourmilliere 
                                        {
                                            explorerCasesVoisines = true;
                                        }
                                    }
                                    else
                                    {
                                        cout << "Apparemment, tout n'est pas ok dans la fourmilliere qu'il y a sur la case\n";
                                    }
                                }
                                else // si il n'y a pas de fourmilliere sur la case. 
                                {
                                    if(positionFourmiSurCase != -1) // si la fourmi ne pense pas etre dans une fourmilliere
                                    {
                                        explorerCasesVoisines = true;
                                    }
                                    else
                                    {
                                        cout << "La fourmi pense etre dans fourmilliere sauf que il n'y a pas de fourmilliere sur sa case !! \n";
                                    }
                                }
                                

                                if(explorerCasesVoisines)
                                {
                                    bool pasDeProbleme = true;

                                    int indiceDirectionCaseVoisine = 0;
                                    while(pasDeProbleme && indiceDirectionCaseVoisine < NBRCASESVOISINES)
                                    {
                                        pasDeProbleme = caseActuelle->GetCaseVoisine(indiceDirectionCaseVoisine)->toutEstOkSurCaseDebutAction();

                                        indiceDirectionCaseVoisine++;
                                    }

                                    if(pasDeProbleme)
                                    {
                                        toutEstOk = true;
                                    }
                                    else
                                    {
                                        cout << "Apparemment, tout n'est pas ok sur au moins une des cases voisines de la fourmi\n";
                                    }
                                }

                            }
                            else
                            {
                                cout << "Apparemment, tout n'est pas ok sur la case de la fourmi\n";
                            }
                        }
                        else
                        {
                            cout << "La fourmi est sur une case IMPRATICABLE ou alors la fourmi est dans une fourmilliere qui est elle-même sur une case IMPRATICABLE\n";
                        }
                    }
                    else
                    {
                        cout << "la case actuelle de la fourmi est nullptr !!\n";
                    }  
               }
               else
               {
                   cout << "probleme sur les indices listesloyales, fourmilliere, positionSurCase\n";
               }
        }
        else
        {
            cout << "jauge de fatigue : " << jaugeDeFatigue << "; ageFourmi : " << ageFourmi << "; nbrActionsDepuisAlerte : " << nbrActionsDepuisAlerte << "\n";
            cout << "probleme sur jauge de fatigue ou age de la fourmi ou nbrActionsDepuisAlerte !!\n";
        }
    }
    else
    {
        cout << "jabot social levels : " << jabotSocialLevel << "; carry level : " << carrylevel << "\n";
        cout << "probleme sur les jabot social levels ou carry level de la fourmi !! \n";
    }



    return toutEstOk;
}

/***************************************************************************//**
 * Augmente le nombre de point d'action de la fourmi en fonction du niveau de fatigue et de l'état
 ******************************************************************************/
void Fourmi::rechargerPointsActionDebutTour()
{
    int pointsActionOctroyed = NBRPAFOURMI;
    
    if(etatFourmi == ALERTED || etatFourmilliere == URGENCEFAMINE)
    {
        pointsActionOctroyed += BONUSNBRPAFOURMIDOPED;
    }

    int maxMalusFatigue = jaugeDeFatigue / PALLIERMALUSFATIGUE;
    int tirageMalusFatigue = uniform_int(0, maxMalusFatigue);
    pointsActionOctroyed -= tirageMalusFatigue;

    if(pointsActionOctroyed < MINIMUMPAOCTROYED)
    {
        pointsActionOctroyed = MINIMUMPAOCTROYED;
    }

    pointsAction += pointsActionOctroyed;
}


/***************************************************************************//**
 * Choisi l'action que la fourmi doit prendre, en fonction de l'état, de la case actuelle et des cases voisines.
 ******************************************************************************/
DecisionFourmi Fourmi::debutActionReconnaissanceEtPriseDeDecision()
{
    DecisionFourmi decisionPrise = ERREURDECISIONELLE;

    if(positionFourmiSurCase == -1) // si la fourmi est dans une fourmilliere, alors son etat est forcément ATENDRE (sinon erreur détectée par tests de début d'action)
    {
        bool pasDeProbleme = true;
        etatFourmilliere = caseActuelle->GetFourmilliereCase()->GetEtatFourmilliere(); // On récupère l'état de la fourmilliere
        if(AFFICHERINFOSDEVELOPPEUR)
        {
            cout << "etat fourmilliere : " << etatFourmilliere << "\n";
        }

        if(etatFourmilliere == REINEMORTE)
        {
            etatFourmi = ZOMBIE;
           // cout << "fourmi devenue zombie\n";
        }
        else if(etatFourmilliere == SURVIEULTIME)
        {
            etatFourmi = ATTENDRE;
        }
        else if(etatFourmilliere == INCROYABLEGOPONDRE || etatFourmilliere == TOUTVABIEN || etatFourmilliere == FAMINE || etatFourmilliere == URGENCEFAMINE)
        {
            if(caseActuelle->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // Si il est possible de sortir de la fourmilliere
            {
                if(etatFourmilliere == URGENCEFAMINE) // Si c'est une urgence absolue, on ne se pose pas de question : on va chercher de la bouffe en mode dopée meme si on est fatigué
                {
                    etatFourmi = CHERCHERBOUFFE;
                }
                else if(etatFourmilliere == INCROYABLEGOPONDRE || etatFourmilliere == TOUTVABIEN || etatFourmilliere == FAMINE) // si etatFourmilliere = INCROYABLEGOPONDRE ou TOUTVABIEN ou FAMINE on regarde le niveau de fatigue avant de sortir
                {
                    if(jaugeDeFatigue > 0) // si on est pas fatigué alors on va chercher de la bouffe sans se poser de questions !
                    {
                        double tirageReposOuChercherBouffe = uniform(0, 1);
                        etatFourmi = ATTENDRE;

                        if(etatFourmilliere == INCROYABLEGOPONDRE)
                        {
                            if(tirageReposOuChercherBouffe < PROBACHERCHERBOUFFEQUANDFATIGUATEDETINCROYABLEGOPONDRE)
                            {
                                etatFourmi = CHERCHERBOUFFE; 
                            }
                        }
                        else if(etatFourmilliere == TOUTVABIEN)
                        {
                            if(tirageReposOuChercherBouffe < PROBACHERCHERBOUFFEQUANDFATIGUATEDETTOUTVABIEN)
                            {
                                etatFourmi = CHERCHERBOUFFE; 
                            }
                        }
                        else if(etatFourmilliere == FAMINE)
                        {
                            if(tirageReposOuChercherBouffe < PROBACHERCHERBOUFFEQUANDFATIGUATEDETFAMINE)
                            {
                                etatFourmi = CHERCHERBOUFFE; 
                            }
                        }
                        else // il y a un probleme dans l'état de la fourmilliere
                        {
                            pasDeProbleme = false;
                        }
                    }
                    else
                    {
                        etatFourmi = CHERCHERBOUFFE;
                    }

                }
                else // il y a un probleme dans l'état de la fourmilliere
                {
                    pasDeProbleme = false;
                }
            }
            else // Dans tous les case, si il est imposible de sortir, alors on attend
            {
                etatFourmi = ATTENDRE;
            }
        }
        else // il y a un probleme dans l'état de la fourmilliere
        {
            pasDeProbleme = false;
        }




        if(etatFourmi == ZOMBIE) // si elle est zombie, elle fait un truc random, osef de toutes façons
        {
            if(caseActuelle->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE)
            {
                double tirageDecision = uniform(0, 1);
                if(tirageDecision < 0.5)
                {
                    decisionPrise = RIEN;
                    etatFourmi = ATTENDRE;
                }
                else
                {
                    decisionPrise = SORTIRDEFOURMILLIERE;  
                }
            }
            else // meme en mode zombie, si elle ne peut pas sortir de la fourmilliere, alors elle ne fait rien 
            {
                decisionPrise = RIEN;
                etatFourmi = ATTENDRE;
            }
        }
        else if(etatFourmi == ATTENDRE)
        {
            decisionPrise = RIEN;
            etatFourmi = ATTENDRE;
        }
        else if(etatFourmi == CHERCHERBOUFFE)
        {
            decisionPrise = SORTIRDEFOURMILLIERE;
        }
        else
        {
            pasDeProbleme = false;
        }


        if(!pasDeProbleme)
        {
            cout << "Il y a un probleme lors du changement d'etat de la fourmi. Apparemment probleme sur l'etat de la fourmilliere !!!\n";
            decisionPrise = ERREURDECISIONELLE;
        }





    }
    else // si la fourmi est sur une case alors son état est forcément CHERCHERBOUFFE ou RAMENERBOUFFE ou ALERTED ou ZOMBIE sinon erreur détectée avec les tests de début d'action
    // Les seules actions potentiellement disponibles sont ENTRERDANSFOURMILLIERE ou RAMASSERDELABOUFFE ou ALLERHAUTGAUCHE ou ALLERHAUTDROIT ou ALLERMILIEUDROITE ou ALLERBASDROITE ou ALLERBASGAUCHE ou ALLERMILIEUGAUCHE
    {
        if(etatFourmi != ZOMBIE) // Une fourmi zombie ne communique pas avec les autres fourmis, elle ne fait donc jamais de trophallaxie ni de transmission d'état ou d'infos.
        {
            list<int> listeIndiceDispos;
            for(int indiceFourmiPresenteCase = 0; indiceFourmiPresenteCase < NBRMAXFOURMISPARCASE; indiceFourmiPresenteCase++)
            {
                if(indiceFourmiPresenteCase != positionFourmiSurCase && caseActuelle->GetListeFourmisPresentes()[indiceFourmiPresenteCase]) // Elle ne doit pas communiquer avec elle-même et l'autre fourmi doit être présente
                {
                    listeIndiceDispos.push_back(indiceFourmiPresenteCase);
                }
            }

            while(listeIndiceDispos.size() > 0) // Tant que la communication n'a pas été effectuée avec toutes les autres fourmis présentes
            {   
                list<int>::iterator iterateurListeIndiceDispos = listeIndiceDispos.begin();

                int tirage = uniform_int(0, listeIndiceDispos.size() - 1);
                for(int i = 0; i < tirage; i++)
                {
                    iterateurListeIndiceDispos++;
                }
                
                Fourmi * fourmiPresente = caseActuelle->GetListeFourmisPresentes()[*iterateurListeIndiceDispos];

                if(this->GetIndiceFourmilliere() == fourmiPresente->GetIndiceFourmilliere()) // Si elles ne viennent pas de la même fourmilliere, alors elles se snobent
                {
                    // Trophallaxie
                    int jabotSocialLevelFourmiArrivante = this->GetJabotSocialLevel();
                    int jabotSocialLevelFourmiPresente = fourmiPresente->GetJabotSocialLevel();
                    //cout << "jabots sociaux avant " << jabotSocialLevelFourmiArrivante << " " << jabotSocialLevelFourmiPresente << "\n";
                    this->trophallaxie(jabotSocialLevelFourmiPresente);
                    fourmiPresente->trophallaxie(jabotSocialLevelFourmiArrivante);
                    //cout << "jabots sociaux apres " << jabotSocialLevelFourmiArrivante << " " << jabotSocialLevelFourmiPresente << "\n";


                    // Communication EtatFourmi
                    if(this->GetEtatFourmi() != this->GetEtatFourmi())
                    {
                        if(this->GetEtatFourmi() == ALERTED || fourmiPresente->GetEtatFourmi() == ALERTED)
                        {
                            int recupNbrActionsDepuisAlerte;
                            if(this->GetEtatFourmi() == ALERTED)
                            {
                                recupNbrActionsDepuisAlerte = this->GetNbrActionsDepuisAlerte();
                            }
                            else
                            {
                                recupNbrActionsDepuisAlerte = fourmiPresente->GetNbrActionsDepuisAlerte();
                            }

                            double tirageProbaTransmettreAlerte = uniform(0, 1);

                            if(tirageProbaTransmettreAlerte <= exp((double)(-1 * recupNbrActionsDepuisAlerte) * COEFDURABILITEPROPAGATIONALERTE)) // La fourmi alertée ne transmet pas forcément l'alerte à toutes les fourmis. 
                            {
                                if(this->GetEtatFourmi() == ALERTED)
                                {
                                    fourmiPresente->SetEtatFourmiALERTED(); // elle se dopera au début de son tour
                                }
                                else
                                {
                                    this->SetEtatFourmiALERTED();
                                    pointsAction += BONUSNBRPAFOURMIDOPED; // je me dope direct
                                }
                            }
                        }
                    }
                }
                //cout << "iter avant suppr " << *iterateurListeIndiceDispos << "; taille = " << listeIndiceDispos.size() << "\n";
                listeIndiceDispos.erase(iterateurListeIndiceDispos);
            }
        }


        if(caseActuelle->GetFourmilliereCase() != nullptr && etatFourmi != ZOMBIE) // source d'alerte pour tous les états sauf zombie, etre sur une fourmilliere ennemie
        {
            if(caseActuelle->GetFourmilliereCase()->GetIndiceFourmilliere() != indiceFourmilliere) // si c'est bel et bien une fourmilliere ennemie, elle s'alerte même si elle est déjà alertée
            {
                this->SetEtatFourmiALERTED();
                pointsAction += BONUSNBRPAFOURMIDOPED; // je me dope direct
            }
        }



        bool faireAnalyseDePheromones = true; // Certaines situations permettent de prendre des décisions sans faire d'analyse de phéromones
        if(caseActuelle->GetBouffeQuantity() > 0 && etatFourmi == CHERCHERBOUFFE)
        {
            decisionPrise = RAMASSERDELABOUFFE;
            faireAnalyseDePheromones = false;
        }
        else if(etatFourmi == ALERTED || etatFourmi == RAMENERBOUFFE || (etatFourmi == CHERCHERBOUFFE && jabotSocialLevel < COEFNIVEAUJABOTSOCIALDEMITOUR * MAXJABOTSOCIALLEVELFOURMI))
        {
            if(caseActuelle->GetFourmilliereCase() != nullptr)
            {
                if(caseActuelle->GetFourmilliereCase()->GetIndiceFourmilliere() == indiceFourmilliere) // si elle est sur une fourmilliere amie (il y aura forcément la place pour rentrer puisque la fourmilliere garantit une place pour toutes ses fourmis loyales)
                {
                    decisionPrise = ENTRERDANSFOURMILLIERE;
                    faireAnalyseDePheromones = false;
                }  
            }
        }



        if(faireAnalyseDePheromones) // si faireAnalyseDePheromones == false, alors normalement une décision a déjà été prise, sinon ça veut dire qu'il y a un problème
        {
            bool analyseConcluante = false; // Une analyse concluante permet de prendre une décision immédiate
            if(etatFourmi != ZOMBIE) // On rappelle qu'une fourmi zombie ne fait pas d'analyse de phéromones
            {
                // analyse de phéromones de la case. Si analyse concluante : analyseConcluante = true;
                Direction directionSympa;
                bool directionSympaChoisie = false;

                if(etatFourmi == RAMENERBOUFFE || etatFourmi == ALERTED || jabotSocialLevel < COEFNIVEAUJABOTSOCIALDEMITOUR * MAXJABOTSOCIALLEVELFOURMI)
                {
                    if(caseActuelle->GetNbrPheromonesAmis(FOURMILLIERE, indiceFourmilliere) > 0)
                    {
                        Direction directionMax = HAUTGAUCHE;
                        int max = caseActuelle->GetNbrPheromonesAmis(FOURMILLIERE, directionMax, indiceFourmilliere);

                        for(int direction = 1; direction < NBRCASESVOISINES; direction++)
                        {
                            if(caseActuelle->GetNbrPheromonesAmis(FOURMILLIERE, (Direction)direction, indiceFourmilliere) > max)
                            {
                                directionMax = (Direction)direction;
                                max = caseActuelle->GetNbrPheromonesAmis(FOURMILLIERE, (Direction)direction, indiceFourmilliere);
                            }
                        }

                        directionSympa = directionMax;
                        directionSympaChoisie = true;
                    }
                }
                else // si etatFourmi == CHERCHERBOUFFE et que son jabot social level est au-dessus de COEFNIVEAUJABOTSOCIALDEMITOUR * MAXJABOTSOCIALLEVELFOURMI
                {




                    //choix des dorectoin des phéromones
                    list<Direction> directionsSympas;


                    int maxpheromonebouffe = 0;
                    for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                    {
                        bool alerteDanscetteDirection = false;
                        bool bouffeDanscetteDirection = false;

                        if(caseActuelle->GetNbrPheromonesAmis(ALERTE, (Direction)direction, indiceFourmilliere) > 0)
                        {
                            alerteDanscetteDirection = true;
                        }

                        if(!alerteDanscetteDirection && caseActuelle->GetNbrPheromones(BOUFFE, (Direction)direction) > 0) // On peut éventuellement trouver de la bouffe grace à des phéromones ennemies
                        {

                            bouffeDanscetteDirection = true;
                            int temp = caseActuelle->GetNbrPheromones(BOUFFE, (Direction)direction);
                            if(AFFICHERINFOSDEVELOPPEUR)
                            {
                                cout << "direction sympa : " << direction << "\n";
                            }

                            if (temp > maxpheromonebouffe)
                            {
                                directionsSympas.clear();
                                maxpheromonebouffe = temp;
                                directionSympaChoisie = true;
                                
                                directionsSympas.push_back((Direction)direction);
                            }
                            else
                            {
                                if (temp = maxpheromonebouffe)
                                {
                                    directionsSympas.push_back((Direction)direction);
                                }
                            }
                        }

                    }


                    
                        if(directionsSympas.size() > 0)
                        {
                            int tirageDirection = uniform_int(0, directionsSympas.size() - 1);

                            list<Direction>::iterator iterateurListeDirectionsSympas = directionsSympas.begin();

                            for(int i = 0; i < tirageDirection; i++)
                            {
                                iterateurListeDirectionsSympas++;
                            }

                            directionSympa = *iterateurListeDirectionsSympas;
                            directionSympaChoisie = true;
                    }
                    
                }



                // On vérifie si la direction choisie est cohérente
                if(directionSympaChoisie)
                {
                    if(directionSympa == HAUTGAUCHE)
                    {
                        if(caseActuelle->GetCaseVoisine(HAUTGAUCHE) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case HAUTGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERHAUTGAUCHE;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else if(directionSympa == HAUTDROIT)
                    {
                        if(caseActuelle->GetCaseVoisine(HAUTDROIT) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(HAUTDROIT)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(HAUTDROIT)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case HAUTDROIT existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERHAUTDROIT;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else if(directionSympa == MILIEUDROITE)
                    {
                        if(caseActuelle->GetCaseVoisine(MILIEUDROITE) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(MILIEUDROITE)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(MILIEUDROITE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case MILIEUDROITE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERMILIEUDROITE;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else if(directionSympa == BASDROITE)
                    {
                        if(caseActuelle->GetCaseVoisine(BASDROITE) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(BASDROITE)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(BASDROITE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case BASDROITE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERBASDROITE;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else if(directionSympa == BASGAUCHE)
                    {
                        if(caseActuelle->GetCaseVoisine(BASGAUCHE) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(BASGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(BASGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case BASGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERBASGAUCHE;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else if(directionSympa == MILIEUGAUCHE)
                    {
                        if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE) != nullptr)
                        {
                            if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                            {
                                if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case MILIEUGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                                {
                                    decisionPrise = ALLERMILIEUGAUCHE;
                                    analyseConcluante = true;
                                }
                            }
                        }
                    }
                    else // au cas où 
                    {
                        decisionPrise = ERREURDECISIONELLE;
                        analyseConcluante = true;
                    }
                }
            }




            
            if(!analyseConcluante) // si l'analyse n'est pas concluante (fourmi zombie ou alors fourmi perdue), alors on tire une action au sort une action réalisable
            {


                list<DecisionFourmi> listeDecisionsCoherentesDisponibles;

                list<int> Nombrealeatoire;
                int Max_Nombrealeatoire=1;
                int Somme_Nombrealeatoire=0;

                list<int> Nombrepheromonerentre;
                int Somme_Nombrepheromonerentre=0;

                list<int> Niveaudedifficulte;
                int Somme_Niveaudedifficulte=0;
                int facteurNiveaudedifficulte = 0;

                if(caseActuelle->GetCaseVoisine(HAUTGAUCHE) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case HAUTGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERHAUTGAUCHE);
                            
                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                if(caseActuelle->GetCaseVoisine(HAUTDROIT) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(HAUTDROIT)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(HAUTDROIT)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case HAUTDROIT existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERHAUTDROIT);
                            
                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                if(caseActuelle->GetCaseVoisine(MILIEUDROITE) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(MILIEUDROITE)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(MILIEUDROITE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case MILIEUDROITE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERMILIEUDROITE);

                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                if(caseActuelle->GetCaseVoisine(BASGAUCHE) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(BASGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(BASGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case BASGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERBASGAUCHE);

                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                if(caseActuelle->GetCaseVoisine(BASDROITE) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(BASDROITE)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(BASDROITE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case BASDROITE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERBASDROITE);

                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE) != nullptr)
                {
                    if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE)->GetPaysageCase() != IMPRATICABLE)
                    {
                        if(caseActuelle->GetCaseVoisine(MILIEUGAUCHE)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE) // si la case MILIEUGAUCHE existe, est praticable et n'est pas encore pleine, alors il est possible d'y aller
                        {
                            listeDecisionsCoherentesDisponibles.push_back(ALLERMILIEUGAUCHE);

                            int temp0 = uniform_int(1, Max_Nombrealeatoire);
                            Nombrealeatoire.push_back( temp0 );
                  
                            int temp1 = 0;
                            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
                            {
                                    temp1 = temp1 + caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetNbrPheromones(FOURMILLIERE, (Direction)direction);
                            }
                            
                            Nombrepheromonerentre.push_back(temp1);
                            Niveaudedifficulte.push_back(10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte);
                            Somme_Nombrealeatoire += temp0;
                            Somme_Nombrepheromonerentre += temp1;
                            Somme_Niveaudedifficulte += 10 - (int)caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() + facteurNiveaudedifficulte;
                        }
                    }
                }
                

                int nbrDecisionsDisponibles = listeDecisionsCoherentesDisponibles.size();
                if(nbrDecisionsDisponibles > 0)
                {
                    
                    list<double> Facteurdecision;


                    list<int>::iterator iterateurNombrealeatoire = Nombrealeatoire.begin();
                    list<int>::iterator iterateurNombrepheromonerentre = Nombrepheromonerentre.begin();
                    list<int>::iterator iterateurNiveaudedifficulte = Niveaudedifficulte.begin();

                    for(int i = 0; i < nbrDecisionsDisponibles; i++)
                    {
                        //cout << *iterateurNombrealeatoire + *iterateurNombrepheromonerentre + *iterateurNiveaudedifficulte << " ";
                        //cout << (Somme_Niveaudedifficulte + Somme_Nombrepheromonerentre + Somme_Nombrealeatoire) << " ";
                        double tempfacteur = ( *iterateurNombrealeatoire + *iterateurNombrepheromonerentre + *iterateurNiveaudedifficulte +0.0) / (Somme_Niveaudedifficulte + Somme_Nombrepheromonerentre + Somme_Nombrealeatoire);

                        //cout << tempfacteur << endl;
                        iterateurNombrealeatoire++;
                        iterateurNombrepheromonerentre++;
                        iterateurNiveaudedifficulte++;

                        Facteurdecision.push_back(tempfacteur);
                    }
                    //cout <<endl<<endl;

                    list<double>::iterator iterateurFacteurdecision = Facteurdecision.begin();

                    double tirageDecision = uniform_int(0, 1000)/1000.0;

                    list<DecisionFourmi>::iterator iterateurListeDecisionsDisponibles = listeDecisionsCoherentesDisponibles.begin();

                    double sommefacteur = *iterateurFacteurdecision;
                    iterateurFacteurdecision++;
                    while (sommefacteur < tirageDecision)
                    {
                        //cout << sommefacteur << " ";
                        sommefacteur += *iterateurFacteurdecision;
                        iterateurFacteurdecision++;
                        iterateurListeDecisionsDisponibles++;
                    }
                    //cout << endl;


                    decisionPrise = *iterateurListeDecisionsDisponibles;
                }
                else // Si la fourmi ne peut rien faire, alors elle fait l'action par défaut qui est RAMASSERDELABOUFFE
                {
                    decisionPrise = RAMASSERDELABOUFFE;
                }

            }
            
        }
        




        if(decisionPrise != ERREURDECISIONELLE && decisionPrise != RIEN && decisionPrise != SORTIRDEFOURMILLIERE) // à ce stade-là, ne pas avoir pris de décision ou alors RIEN ou SORTIRDEFOURMILLIERE est considéré comme une erreur
        {
            // Contrôle du fait qu'elle ne meure pas de faim en faisant cette action.
            if(decisionPrise != RAMASSERDELABOUFFE) // si l'action RAMASSERDELABOUFFE la fait mourir de faim, bah tant pis on ne peut plus rien pour elle mdr
            {
                int coutDecisionPrise;
                if(decisionPrise == ENTRERDANSFOURMILLIERE)
                {
                    coutDecisionPrise = ENTREEOUSORTIEFOURMILLIERE;
                }
                else if(decisionPrise == ALLERHAUTGAUCHE)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(HAUTGAUCHE)->GetPaysageCase() * BOUGERDE1PA;
                }
                else if(decisionPrise == ALLERHAUTDROIT)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(HAUTDROIT)->GetPaysageCase() * BOUGERDE1PA;
                }
                else if(decisionPrise == ALLERMILIEUDROITE)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(MILIEUDROITE)->GetPaysageCase() * BOUGERDE1PA;
                }
                else if(decisionPrise == ALLERBASDROITE)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(BASDROITE)->GetPaysageCase() * BOUGERDE1PA;
                }
                else if(decisionPrise == ALLERBASGAUCHE)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(BASGAUCHE)->GetPaysageCase() * BOUGERDE1PA;
                }
                else if(decisionPrise == ALLERMILIEUGAUCHE)
                {
                    coutDecisionPrise = caseActuelle->GetCaseVoisine(MILIEUGAUCHE)->GetPaysageCase() * BOUGERDE1PA; 
                }
                else // au cas où 
                {
                    decisionPrise = ERREURDECISIONELLE;
                }


                if(jabotSocialLevel - coutDecisionPrise < 0) // Si faire cette action la fera immédiatement mourir de faim
                {
                    if(carrylevel > 0 || caseActuelle->GetBouffeQuantity() > 0) // et que ramasser de la bouffe pourrait la faire survivre : on ramasse et sinon tant pis elle fera ce qui était prévu à la base et mourra
                    {
                        decisionPrise = RAMASSERDELABOUFFE;
                    }
                }
            }
        }
        else // au cas où
        {
            decisionPrise = ERREURDECISIONELLE;
        }
    }

    




    return decisionPrise;
}



/***************************************************************************//**
 * détermine si la fourmi survie se tour
 ******************************************************************************/
bool Fourmi::survieFourmi()
{
    bool survieFourmi = false;

    //if(jabotSocialLevel >= (-1) * NBRTOURSSANSMANGEREXPLORATIONDESESPEREEAVANTMORT * NOURRIRFOURMIINACTIVE) // Une fourmi inactive peut survivre plus longtemps sans manger qu'une fourmi active
    //{
    if(jabotSocialLevel >= 0) // Une fourmi ayant un foodLevel négatif meurt instantanément de faim
    {
        if(ageFourmi < AGEMAXFOURMIS)
        {
            double tirageMort = uniform(0, 1);
            if(tirageMort <= PROBASURVIE) // elle peut mourir de façon random avant chaque action
            {
                survieFourmi = true;
            }
            else
            {
             //   cout << "mort random\n";
            }
        }
        else{
           // cout << "mort de vieillesse\n";
        }
    }
    else
    {
       // cout << "mort de faim\n";
    }

    return survieFourmi;
}


void Fourmi::vieillirFourmi(){ageFourmi++;}



void Fourmi::resterInactiveDansFourmilliere()
{
    // Vérification si la décision est cohérente
    if(positionFourmiSurCase == -1)
    {
        // Consommation bouffe, points d'action et jauge de fatigue
        jabotSocialLevel -= RESTERINACTIVE;
        pointsAction--;
        jaugeDeFatigue += 0;

        // Effet de l'action
        jaugeDeFatigue -= RECUPJAUGEDEFATIGUE;
        if(etatFourmilliere != SURVIEULTIME)
        {
            caseActuelle->GetFourmilliereCase()->nourrirFourmiInactive(); // retrait de bouffe des réserves de la fourmilliere
            if(caseActuelle->GetFourmilliereCase()->GetNiveauBouffe() - RESTERINACTIVE >= 0)
            {
                jabotSocialLevel += RESTERINACTIVE;
            }
        }
    }
    else
    {
        cout << "La fourmi Essaie de rester inactive alors qu'elle sait qu'elle n'est pas dans une fourmilliere !\n";
    }
}



void Fourmi::entrerDansFourmilliere()
{
    // Vérification si la décision est cohérente
    if(caseActuelle->GetFourmilliereCase() != nullptr)
    {
        if(positionFourmiSurCase != -1)
        {
            if(caseActuelle->GetFourmilliereCase()->GetIndiceFourmilliere() == indiceFourmilliere)
            {
                if(caseActuelle->GetFourmilliereCase()->GetNbrfourmisPresentes() < NBRMAXFOURMISPARFOURMILLIERE)
                {
                    // Consommation bouffe, points d'action et jauge de fatigue
                    jabotSocialLevel -= ENTREEOUSORTIEFOURMILLIERE;
                    pointsAction--;
                    jaugeDeFatigue += 1;

                    // Effet de l'action
                    if(caseActuelle->retirerFourmi(this))
                    {
                        if(caseActuelle->GetFourmilliereCase()->entreeFourmi(this))
                        {
                            positionFourmiSurCase = -1;
                            etatFourmi = ATTENDRE;

                            // La fourmi entrante dépose automatiquement dans les réserves de la fourmilliere toute la bouffe qu'elle transporte (Cette bouffe a été déposée dans les réserves de la fourmilliere avec la méthode entreeFourmi() de la fourmilliere)
                            if(jabotSocialLevel >= 0)
                            {
                                jabotSocialLevel = 0;
                            }
                            carrylevel = 0;
                        }
                        else
                        {
                            cout << "Erreur, la fourmi a été retirée de la case mais n'a pas réussi à rentrer dans la fourmilliere\n";
                        }
                    }
                    else
                    {
                        cout << "La fourmi veut entrer dans la fourmilliere mais n'a pas réussi à etre retirée de la case\n";
                    }
                }
                else
                {
                    cout << "La fourmi essaie de rentrer dans une fourmilliere alors qu'il n'y a plus de place\n";
                }
            }
            else
            {
                cout << "La fourmi essie de rentrer dans une fourmilliere étrangère lol. Entrée annulée\n";
            }
        }
        else
        {
            cout << "La fourmi Essaie de rentrer dans la fourmilliere alors qu'elle sait qu'elle est déjà dedans !\n";
        }
    }
    else
    {
        cout << "La fourmi essaie de rentrer dans une fourmilliere alors qu'il n'y en a pas sur sa case actuelle\n";
    }
}


void Fourmi::sortirDeFourmilliere()
{
    // Vérification si la décision est cohérente
    if(positionFourmiSurCase == -1)
    {
        if(caseActuelle->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE)
        {
            // Consommation bouffe, points d'action et jauge de fatigue
            jabotSocialLevel = jabotSocialLevel - ENTREEOUSORTIEFOURMILLIERE + caseActuelle->GetFourmilliereCase()->quantityProvisionsAutorisedPourSortie();
                    // La fourmi entrante prend automatiquement dans les réserves de la fourmilliere toute la bouffe dont elle a besoin pour survivre à l'extérieur (Cette bouffe 
                    // va être prise dans les réserves de la fourmilliere avec la méthode sortieFourmi() de la fourmilliere)
            pointsAction--;
            jaugeDeFatigue += 1;

            // Effet de l'action
            if(caseActuelle->GetFourmilliereCase()->sortieFourmi(this))
            {
                int indiceAjoutFourmi = caseActuelle->ajouterFourmi(this);
                if(indiceAjoutFourmi != -1)
                {
                    positionFourmiSurCase = indiceAjoutFourmi;

                }
                else
                {
                    cout << "La fourmi a réussi à sortir de la fourmilliere mais n'a pas réussi à être ajoutée à la case\n";
                }
            }
            else
            {
                cout << "Erreur, la fourmi n'a pas réussi à sortir de la fourmilliere !\n";
            }

        }
        else
        {
            cout << "La fourmi essaie de sortir de sa fourmilliere alors que la case sur laquelle elle est placée est déjà pleine\n";
        }
    }
    else
    {
        cout << "La fourmi Essaie de sortir d'une fourmilliere alors qu'elle sait qu'elle n'est pas dedans !\n";
    }
}



void Fourmi::changerDeCase(Direction directionNouvelleCase)
{
    if(caseActuelle->GetCaseVoisine(directionNouvelleCase) != nullptr)
    {
        // Vérification si la décision est cohérente
        if(positionFourmiSurCase != -1)
        {
            if(caseActuelle->GetCaseVoisine(directionNouvelleCase)->GetPaysageCase() != IMPRATICABLE)
            {
                if(caseActuelle->GetCaseVoisine(directionNouvelleCase)->GetNbrFourmisPresentes() < NBRMAXFOURMISPARCASE)
                {
                    // Consommation bouffe, points d'action et jauge de fatigue
                    jabotSocialLevel -= BOUGERDE1PA * caseActuelle->GetCaseVoisine(directionNouvelleCase)->GetPaysageCase();
                    pointsAction -= caseActuelle->GetCaseVoisine(directionNouvelleCase)->GetPaysageCase();
                    jaugeDeFatigue += 1;

                    // Effet de l'action
                    if(caseActuelle->retirerFourmi(this))
                    {
                        int indiceAjoutFourmi = caseActuelle->GetCaseVoisine(directionNouvelleCase)->ajouterFourmi(this);
                        if(indiceAjoutFourmi != -1)
                        {
                            caseActuelle = caseActuelle->GetCaseVoisine(directionNouvelleCase);
                            positionFourmiSurCase = indiceAjoutFourmi;

                            // Si ce n'est pas une fourmi zombie, et que elle est pas en train de rentrer à cause de la faim, alors on ajoute un phéromone
                            if(etatFourmi != ZOMBIE && !(etatFourmi == CHERCHERBOUFFE && jabotSocialLevel < COEFNIVEAUJABOTSOCIALDEMITOUR * MAXJABOTSOCIALLEVELFOURMI))
                            {
                                TypePheromone typeDuPheromone;
                                if(etatFourmi == CHERCHERBOUFFE)
                                {
                                    typeDuPheromone = FOURMILLIERE;
                                }
                                else if(etatFourmi == RAMENERBOUFFE)
                                {
                                    typeDuPheromone = BOUFFE;
                                }
                                else if(etatFourmi == ALERTED)
                                {
                                    typeDuPheromone = ALERTE;
                                }
                                else
                                {
                                    cout << "ERREUR !! Une fourmi à l'extérieur n'est pas dans le bon état !!!! \n";
                                }
                                   
                                Pheromone nouveauPheromone(typeDuPheromone, indiceFourmilliere);

                                Direction directionPheromone = (Direction)(((int)directionNouvelleCase + 3) % NBRCASESVOISINES); // On pointe l'opposé de là où on arrive
                                if(AFFICHERINFOSDEVELOPPEUR)
                                {
                                    cout << " direction pheromone déposé : " << directionPheromone << "\n";
                                }

                                caseActuelle->ajouterPheromone(nouveauPheromone, directionPheromone);
                                    
                                //}

                            }
                        }
                        else
                        {
                            cout << "erreur lors de l'ajout de la fourmi à la case voisine\n";
                        }
                    }
                    else
                    {
                        cout << "erreur lors du retrait de la fourmi de la case\n";
                    }
                }
                else
                {
                    cout << "La fourmi essaie de rentrer sur une case déjà pleine\n";
                }
            }
            else
            {
                cout << "On essaie de déplacer une fourmi dans une case impraticable !! \n";
            }
        }
        else
        {
            cout << "La fourmi est dans la fourmilliere alros qu'elle est dans une fourmilliere !! \n";
        }
    }
    else
    {
        cout << "On essaie de déplacer une fourmi dans une case qui n'existe pas (sûrement en dehors de la map)\n";
    }
    
}



void Fourmi::ramasserBouffe()
{
    // Vérification si la décision est cohérente
    if(positionFourmiSurCase != -1)
    {
        // Consommation bouffe, points d'action et jauge de fatigue
        jabotSocialLevel -= RAMASSERBOUFFE;
        pointsAction --;
        jaugeDeFatigue += 1;

        // Effet de l'action
        if(caseActuelle->GetBouffeQuantity() > 0)
        {
            if(caseActuelle->GetBouffeQuantity() <= MAXJABOTSOCIALLEVELFOURMI - jabotSocialLevel) // Je prends toute la bouffe de la case pour la mettre dans mon foodLevel et j'en reste là
            {
                jabotSocialLevel += caseActuelle->GetBouffeQuantity();
                caseActuelle->SetBouffeQuantity(0); 
            }
            else // dans tous les cas je vais être rassasié
            {
                if(caseActuelle->GetBouffeQuantity() - MAXJABOTSOCIALLEVELFOURMI + jabotSocialLevel <= MAXCARRYLEVELFOURMI - carrylevel)
                {
                    carrylevel += caseActuelle->GetBouffeQuantity() - MAXJABOTSOCIALLEVELFOURMI + jabotSocialLevel;
                    jabotSocialLevel = MAXJABOTSOCIALLEVELFOURMI;
                    caseActuelle->SetBouffeQuantity(0);
                }
                else 
                {
                    if(etatFourmi == CHERCHERBOUFFE) // même en prenant le maximum possible, il reste toujours de la bouffe sur cette case. Si on est en mode chercherBouffe, on peut passer en mode RAMENERBOUFFE
                    {
                        etatFourmi = RAMENERBOUFFE;
                    }




                    caseActuelle->SetBouffeQuantity(caseActuelle->GetBouffeQuantity() - MAXJABOTSOCIALLEVELFOURMI + jabotSocialLevel - MAXCARRYLEVELFOURMI + carrylevel);
                    jabotSocialLevel = MAXJABOTSOCIALLEVELFOURMI;
                    carrylevel = MAXCARRYLEVELFOURMI;
                }
            }
        }

        // Dans tous les cas je fais en sorte de remplir au max le jabotSocialLevel en puisant dans carryLevel. Si déjà rassasié, ça ne changera rien.
        if(carrylevel <= MAXJABOTSOCIALLEVELFOURMI - jabotSocialLevel)
        {
            jabotSocialLevel += carrylevel;
            carrylevel = 0;
        }
        else
        {
            carrylevel -= MAXJABOTSOCIALLEVELFOURMI - jabotSocialLevel;
            jabotSocialLevel = MAXJABOTSOCIALLEVELFOURMI;
        }
    }
    else
    {
        cout << "La fourmi essaie de ramasserBouffe en prenant la bouffe de la case alors qu'elle est dans une fourmilliere\n";
    }
}



/***************************************************************************//**
 * Si cela est possible, réalise un échange de nourriture entre les 2 fourmi
 *
 * @param jabotSocialLevelAutreFourmi Niveau du jabot 
 ******************************************************************************/
void Fourmi::trophallaxie(int jabotSocialLevelAutreFourmi)
{
    // cast d'un float en int, ça arrondi à la valeur dont la valeur absolue est la plus basse.
    int niveauJabotSocialDeclenchantTrophallaxieRecevante = MAXJABOTSOCIALLEVELTROPHALLAXIERECEVANTE * MAXJABOTSOCIALLEVELFOURMI;
    int niveauJabotSocialDeclenchantTrophallaxieDonnante = MINJABOTSOCIALLEVELTROPHALLAXIEDONNANTE * MAXJABOTSOCIALLEVELFOURMI;

    if(jabotSocialLevel < niveauJabotSocialDeclenchantTrophallaxieRecevante || jabotSocialLevelAutreFourmi < niveauJabotSocialDeclenchantTrophallaxieRecevante) // si une fourmi réclame une trophallaxie
    {
        if(jabotSocialLevel >= niveauJabotSocialDeclenchantTrophallaxieDonnante || jabotSocialLevelAutreFourmi >= niveauJabotSocialDeclenchantTrophallaxieDonnante) // si une fourmi est en mesure de faire une trophallaxie donnante
        {
            int deltaJabotsSociaux = jabotSocialLevel - jabotSocialLevelAutreFourmi;
            if(deltaJabotsSociaux < 0) // valeur absolue
            {
                deltaJabotsSociaux *= -1;
            }

            deltaJabotsSociaux *= COEFREDUCTIONDELTAJABOTSOCIALAPRESTROPHALLAXIE;
            deltaJabotsSociaux = (deltaJabotsSociaux + 1) * 0.5 ;

            if(jabotSocialLevel <= jabotSocialLevelAutreFourmi)
            {
                jabotSocialLevel += deltaJabotsSociaux;
            }
            else
            {
                jabotSocialLevel -= deltaJabotsSociaux;
            }
        }
    }
}


void Fourmi::SetEtatFourmiALERTED() // etatFourmi = alerted et reset le nbrActionsDepuisAlerte 
{
    etatFourmi = ALERTED;
    nbrActionsDepuisAlerte = 0;
}


