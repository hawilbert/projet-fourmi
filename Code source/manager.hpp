#ifndef MANAGER_HPP
#define MANAGER_HPP

#include "general.hpp"

#include "caseMap.hpp"
#include "mersenneTwister.hpp"
#include "fourmilliere.hpp"


class CaseMap;
class Fourmilliere;

class Manager
{
    private :
        CaseMap*** map;
        Fourmilliere** listeFourmillieres;

    public :
        Manager();
        ~Manager();

        CaseMap*** GetMap();
        Fourmilliere ** GetListeFourmilliere();

        void initialiserMap();
        void initialiserListeFourmillieres();
        
        void actionsDeDebutDeTour();

        void majMapBouffe();
        void majMapPheromone();

        void afficherMapHexa();
};



#endif
