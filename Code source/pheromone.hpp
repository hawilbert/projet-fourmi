#ifndef PHEROMONE_HPP
#define PHEROMONE_HPP

#include "general.hpp"

class Pheromone
{
    private:
        TypePheromone typePheromone;
        int agePheromone;
        int signatureFourmillierePheromone;

    public:
        Pheromone(TypePheromone, int);

        TypePheromone GetTypePheromone();
        int GetAgePheromone();
        
        void faireVieillirPheromone();
        int GetSignatureFourmillierePheromone();
};



#endif
