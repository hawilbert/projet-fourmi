#include "manager.hpp"


Manager::Manager()
{
    cout << "Je construis un manager\n";

    map = new CaseMap**[LONGUEURMAP];
    for(int i = 0; i < LONGUEURMAP; i++)
    {
        map[i] = new CaseMap*[LARGEURMAP];
    }

    listeFourmillieres = new Fourmilliere*[NBRFOURMILLIERES];
}

Manager::~Manager()
{
    if(listeFourmillieres != nullptr)
    {
        for(int i = 0; i < NBRFOURMILLIERES; i++)
        {
            if(listeFourmillieres[i] != nullptr)
            {
                delete listeFourmillieres[i];
            }
        }
        delete [] listeFourmillieres;
    }


    if(map != nullptr)
    {
        for(int i = 0; i < LONGUEURMAP; i++)
        {
            if(map[i] != nullptr)
            {
                for(int j = 0; j < LARGEURMAP; j++)
                {
                    if(map[i][j] != nullptr)
                    {
                        delete map[i][j];
                    }
                }
                delete [] map[i];
            }
        }
        delete [] map;
    }

    cout << "Je détruis un manager\n";
}

CaseMap*** Manager::GetMap(){return map;}
Fourmilliere** Manager::GetListeFourmilliere(){return listeFourmillieres;}

void Manager::initialiserMap()
{
    for(int i = 0; i < LONGUEURMAP; i++)
    {
        for(int j = 0; j < LARGEURMAP; j++)
        {
            PaysageCase paysageNouvelleCase = IMPRATICABLE;
            int bouffeQuantityNouvelleCase = 0;

            if(i != 0 && i != LONGUEURMAP - 1 && j != 0 && j != LARGEURMAP - 1) // si c'est pas sur les bords de la map
            {
                double tiragePaysageCase = uniform(0, 1);

                if(tiragePaysageCase <= 0.5)
                {
                    paysageNouvelleCase = PLAT;
                }
                else if(tiragePaysageCase <= 0.75)
                {
                    paysageNouvelleCase = HERBE;
                }
                else if(tiragePaysageCase <= 0.875)
                {
                    paysageNouvelleCase = ROCHER;
                }
                else if(tiragePaysageCase <= 0.98)
                {
                    paysageNouvelleCase = EAU;
                }

                if(paysageNouvelleCase != IMPRATICABLE) // si le paysage tiré est IMPRATICABLE, alors on ne met pas de bouffe dessus
                {
                    bouffeQuantityNouvelleCase = uniform_int(0, 30);
                    if(bouffeQuantityNouvelleCase == 0)
                    {
                        bouffeQuantityNouvelleCase = 10000;
                    }
                    else
                    {
                        bouffeQuantityNouvelleCase = 0;
                    }
                }
            }
            map[i][j] = new CaseMap(i, j, paysageNouvelleCase, bouffeQuantityNouvelleCase);
        }
    }

    int transformation[NBRCASESVOISINES][2][2] = {{{-1, -1}, {-1, 0}}, // 6 lignes car 6 cases voisines. colonne de gauche pour les lignes paires, à droite pour les impaires. les couples sont coordonnées x,y
                                                  {{-1, 0}, {-1, 1}},
                                                  {{0, 1}, {0, 1}},
                                                  {{1, 0}, {1, 1}},
                                                  {{1, -1}, {1, 0}},
                                                  {{0, -1}, {0, -1}}};


    for(int i = 0; i < LONGUEURMAP; i++)
    {
        for(int j = 0; j < LARGEURMAP; j++)
        {
            bool ligneImpaire = true;
            if(i % 2 == 0)
            {
                ligneImpaire = false;
            }

            for(int direction = 0; direction < NBRCASESVOISINES; direction++)
            {
                int iCaseVoisine = i + transformation[direction][ligneImpaire][0];
                int jCaseVoisine = j + transformation[direction][ligneImpaire][1];

                if(iCaseVoisine >= 0 && iCaseVoisine < LONGUEURMAP && jCaseVoisine >= 0 && jCaseVoisine < LARGEURMAP)
                {
                    map[i][j]->SetCaseVoisine(map[iCaseVoisine][jCaseVoisine], direction);
                }
                else
                {
                    map[i][j]->SetCaseVoisine(nullptr, direction);
                }
            }
        }
    }
}


void Manager::initialiserListeFourmillieres()
{
    for(int indiceFourmilliere = 0; indiceFourmilliere < NBRFOURMILLIERES; indiceFourmilliere++)
    {
        bool boolFourmilliereNonPlaced = true;
        while(boolFourmilliereNonPlaced)
        {
            int iTirage = uniform_int(0, LONGUEURMAP - 1);
            int jTirage = uniform_int(0, LARGEURMAP - 1);

            if(map[iTirage][jTirage]->GetFourmilliereCase() == nullptr && map[iTirage][jTirage]->GetPaysageCase() != IMPRATICABLE)
            {
                boolFourmilliereNonPlaced = false;

                Fourmilliere * nouvelleFourmilliere = new Fourmilliere(indiceFourmilliere, map[iTirage][jTirage]);

                listeFourmillieres[indiceFourmilliere] = nouvelleFourmilliere;
                map[iTirage][jTirage]->SetFourmilliereCase(nouvelleFourmilliere);

                int nbrFourmisPondues = uniform_int(0, 50);
                
                bool boolFourmiPondue = false;
                for(int fourmiPondue = 0; fourmiPondue < nbrFourmisPondues; fourmiPondue++)
                {
                    if(!boolFourmiPondue)
                    {
                        nouvelleFourmilliere->pondreFourmi();
                        //boolFourmiPondue = true;
                    }
                }
            }
        }

        
    }
}

/***************************************************************************//**
 * Joue un tour
 ******************************************************************************/
void Manager::actionsDeDebutDeTour()
{
    for(int fourmilliere = 0; fourmilliere < NBRFOURMILLIERES; fourmilliere++)
    {
        listeFourmillieres[fourmilliere]->playTurnFourmilliere();

        //cout << "fourmilliere " << fourmilliere << " : etatFamine " << listeFourmillieres[fourmilliere]->GetEtatFourmilliere() << "; niveauBouffe : " << listeFourmillieres[fourmilliere]->GetNiveauBouffe() << "; nbrLoyales " << listeFourmillieres[fourmilliere]->GetNbrfourmisLoyales() << "; nbrPresentes " << listeFourmillieres[fourmilliere]->GetNbrfourmisPresentes() << "\n";

    }

    majMapPheromone();
    majMapBouffe();
}




/***************************************************************************//**
 * Regenère la nourriture sur la carte
 ******************************************************************************/
void Manager::majMapBouffe()
{
    int bouffePourRajout = QUANTITYBOUFFERECHARGEMENTMAP;

    while(bouffePourRajout > 0)
    {
        int iCaseRecharged = uniform_int(0, LONGUEURMAP - 1);
        int jCaseRecharged = uniform_int(0, LARGEURMAP - 1);

        if(map[iCaseRecharged][jCaseRecharged]->GetPaysageCase() != IMPRATICABLE)
        {
            int quantityBouffeRajouted = uniform_int(0, QUANTITYBOUFFERECHARGEMENTMAP / MINIMUMDECASESRECHARGED);
            if(quantityBouffeRajouted > bouffePourRajout)
            {
                quantityBouffeRajouted = bouffePourRajout;
            }

            map[iCaseRecharged][jCaseRecharged]->SetBouffeQuantity(map[iCaseRecharged][jCaseRecharged]->GetBouffeQuantity() + quantityBouffeRajouted);
            bouffePourRajout -= quantityBouffeRajouted;
            //cout << quantityBouffeRajouted << "\n";
        }
    }
}


/***************************************************************************//**
 * Viellie les phéromone
 ******************************************************************************/
void Manager::majMapPheromone()
{
    for(int i = 0; i < LONGUEURMAP; i++)
    {
        for(int j = 0; j < LARGEURMAP; j++)
        {
            if(map[i][j]->GetPaysageCase() != IMPRATICABLE)
            {
                map[i][j]->faireVieillirPheromones();
            }
        }
    }
}






void Manager::afficherMapHexa()
{
    cout << "Voici la map actuelle : Longueur = " << LONGUEURMAP << "; largeur = " << LARGEURMAP << "\n";
    
    string couleurCompleteGrille = "\e[1;37m\e[40m";

    for(int i = 0; i < LONGUEURMAP + 1; i++)
    {
        string couleurDefault = "\e[0m";

        string couleurGrilleNormale = "\e[1;37m\e[40m"; // fg = bold white et bg = black
        string couleurGrilleFourmilliereVivante = "\e[0;31m\e[40m"; // fg = red et bg = black

        string couleurPoliceCase = "\e[1;37m"; // fg = bold white
        string couleurPoliceFourmilliereMorte = "\e[31m"; // fg = red 

        string couleurCasePleine = "\e[41m\e[31m"; // fg = red et bg = red

        string couleurPLAT = "\e[43m" + couleurPoliceCase; // bg = yellow (plutôt orange en fait)
        string couleurHERBE = "\e[42m" + couleurPoliceCase; // bg = green
        string couleurROCHER = "\e[0;100m" + couleurPoliceCase; // bg = high intensity black
        string couleurEAU = "\e[44m" + couleurPoliceCase; // bg = blue
        string couleurIMPRATICABLE = "\e[41m" + couleurPoliceCase; // bg = red

        string ligne0pourPrint = "";
        string ligne1pourPrint = "";
        string ligne2pourPrint = "";
        string ligne3pourPrint = "";

        for(int j = 0; j < LARGEURMAP; j++)
        {
            if(i == 0) // premiere ligne
            {
                string couleurGrille = couleurGrilleNormale;
                if(j == 0) // premiere colonne
                {
                    if(map[i][j]->GetFourmilliereCase() != nullptr) // si la case concernée comporte une fourmilliere
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┌─────────";
                }
                else if(j < LARGEURMAP - 1) // pas la premiere ni la derniere colonne
                {
                    if(map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "─────────";
                }
                else // derniere colonne
                {
                    if(map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "─────────┐";
                }
            }
            else if(i % 2 == 0 && i < LONGUEURMAP) // ligne paire mais pas la premiere ligne ni la derniere spéciale
            {
                string couleurGrille = couleurGrilleNormale;
                if(j == 0) // premiere colonne
                {
                    // formeLigne = "┌────┴────"
                    if(map[i][j]->GetFourmilliereCase() != nullptr) // si la case concernée comporte une fourmilliere
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┌────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴────";
                }
                else if(j < LARGEURMAP - 1) // pas la premiere ni la derniere colonne
                {
                    // formeLigne = "┬────┴────┘"
                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴";
                    couleurGrille = couleurGrilleNormale;

                    if( map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                    couleurGrille = couleurGrilleNormale;
                }
                else // derniere colonne
                {
                    // formeLigne = "┬────┴────┬────┘"
                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴";
                    couleurGrille = couleurGrilleNormale;

                    if( map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────┬";
                    couleurGrille = couleurGrilleNormale;

                    if( map[i - 1][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────┘";
                    couleurGrille = couleurGrilleNormale;
                }
            }
            else if(i % 2 == 1 && i < LONGUEURMAP)// ligne impaire
            {
                string couleurGrille = couleurGrilleNormale;
                if(j == 0) // premiere colonne
                {
                    //formeGrille = "└────┬────┴────";
                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr) // si la case concernée comporte une fourmilliere
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "└────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i - 1][j + 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j + 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                }
                else if(j < LARGEURMAP - 1) // pas la premiere ni la derniere colonne
                {
                    //formeGrille = "┬────┴────";
                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i - 1][j + 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j + 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                }
                else // derniere colonne
                {
                    //formeGrille = "┬────┴────┐";
                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┬";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "┴";
                    couleurGrille = couleurGrilleNormale;

                    if(map[i][j]->GetFourmilliereCase() != nullptr) 
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;
                    }
                    ligne0pourPrint += couleurGrille + "────┐";
                }
            }
            else // La toute derniere spéciale
            {
                if(i % 2 == 0) // la toute derniere spéciale est une ligne paire
                {
                    string couleurGrille = couleurGrilleNormale;
                    if(j == 0) // premiere colonne
                    {
                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "     └────";
                    }
                    else if(j < LARGEURMAP - 1) // pas la premiere ni la derniere colonne
                    {
                        if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "─────";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i - 1][j]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "┴";
                        couleurGrille = couleurGrilleNormale;

                        if( map[i - 1][j]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "────";
                        couleurGrille = couleurGrilleNormale;
                    }
                    else // derniere colonne
                    {
                        if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "─────";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j - 1]->GetFourmilliereCase() != nullptr || map[i - 1][j]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "┴";
                        couleurGrille = couleurGrilleNormale;

                        if( map[i - 1][j]->GetFourmilliereCase() != nullptr)
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "─────────┘";
                        couleurGrille = couleurGrilleNormale;
                    }
                }
                else  // la toute derniere spéciale est une ligne impaire
                {
                    string couleurGrille = couleurGrilleNormale;
                    if(j == 0) // premiere colonne
                    {
                        //formeGrille = "└─────────┴────";
                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr) // si la case concernée comporte une fourmilliere
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "└─────────";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i - 1][j + 1]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "┴";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j + 1]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "────";
                    }
                    else if(j < LARGEURMAP - 1) // pas la premiere ni la derniere colonne
                    {
                        //formeGrille = "─────┴────";
                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "─────";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr || map[i - 1][j + 1]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "┴";
                        couleurGrille = couleurGrilleNormale;

                        if(map[i - 1][j + 1]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "────";
                    }
                    else // derniere colonne
                    {
                        //formeGrille = "─────┘";
                        if(map[i - 1][j]->GetFourmilliereCase() != nullptr) 
                        {
                            couleurGrille = couleurGrilleFourmilliereVivante;
                        }
                        ligne0pourPrint += couleurGrille + "─────┘";
                        couleurGrille = couleurGrilleNormale;
                    }
                }
            }
            // affichage des traverses horizontales de la grille effectué

            // Affichons maintenant l'intérieur des cases
            if(i < LONGUEURMAP)
            {
                // On affiche d'abord le bord gauche de la case
                string couleurGrille = couleurGrilleNormale;
                if(j == 0) // premiere colonne
                {
                    if(i % 2 == 1) // si on est sur une ligne impaire
                    {                            
                        ligne1pourPrint += couleurGrille + "     ";
                        ligne2pourPrint += couleurGrille + "     ";
                        ligne3pourPrint += couleurGrille + "     ";
                    }


                    if(map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;                                
                    }                            
                    ligne1pourPrint += couleurGrille + "│";
                    ligne2pourPrint += couleurGrille + "│";
                    ligne3pourPrint += couleurGrille + "│";
                }
                else // toutes les colonnes sauf la premiere
                {
                    if(map[i][j - 1]->GetFourmilliereCase() != nullptr || map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        couleurGrille = couleurGrilleFourmilliereVivante;                                
                    }                            
                    ligne1pourPrint += couleurGrille + "│";
                    ligne2pourPrint += couleurGrille + "│";
                    ligne3pourPrint += couleurGrille + "│";
                }



                // Récupérons ensuite les infos et mettons les en forme :

                // --> Déterminons la couleur de la case
                string couleurCase = "\e[49m"; // background : 41 red; 42 green; 43 yellow; 44 blue; 45 magenta (purple); 46 cyan; 47 white; 49 default (black)
                if(map[i][j]->GetPaysageCase() == PLAT)
                {
                    couleurCase = couleurPLAT;
                }
                else if(map[i][j]->GetPaysageCase() == HERBE)
                {
                    couleurCase = couleurHERBE;
                }
                else if(map[i][j]->GetPaysageCase() == ROCHER)
                {
                    couleurCase = couleurROCHER;
                }
                else if(map[i][j]->GetPaysageCase() == EAU)
                {
                    couleurCase = couleurEAU;
                }
                else if(map[i][j]->GetPaysageCase() == IMPRATICABLE)
                {
                    couleurCase = couleurIMPRATICABLE;
                }

                // --> Mettons ensuite en forme l'affichage du niveau de bouffe
                int bouffeQuantity = map[i][j]->GetBouffeQuantity();
                ligne1pourPrint += couleurCase;

                if(map[i][j]->GetFourmilliereCase() != nullptr)
                {
                    if(map[i][j]->GetFourmilliereCase()->GetEtatFourmilliere() == REINEMORTE)
                    {
                        ligne1pourPrint += couleurPoliceFourmilliereMorte;
                    }
                }

                if(bouffeQuantity < 0)
                {
                    ligne1pourPrint += "ERR   ";
                }
                else if(bouffeQuantity == 0)
                {
                    if(map[i][j]->GetFourmilliereCase() != nullptr)
                    {
                        if(map[i][j]->GetFourmilliereCase()->GetEtatFourmilliere() == REINEMORTE)
                        {
                            ligne1pourPrint += "0     ";
                        }
                        else
                        {
                            int tempbouffe = map[i][j]->GetFourmilliereCase()->GetNiveauBouffe();
                            
                            if (tempbouffe<10000)
                            {
                            if (tempbouffe<1000)
                            {
                                if (tempbouffe<100)
                            {
                                if (tempbouffe<10)
                            {
                                ligne1pourPrint += to_string(tempbouffe)+ "     ";
                            }
                            else
                            {
                                ligne1pourPrint += to_string(tempbouffe)+ "    ";
                            }
                            }
                            else
                            {
                                ligne1pourPrint += to_string(tempbouffe)+ "   ";
                            }
                            }
                            else
                            {
                                ligne1pourPrint += to_string(tempbouffe)+ "  ";
                            }
                            }
                            else
                            {
                                ligne1pourPrint += ">10e5 ";
                            }
                            
                        }
                    }
                    else
                    {
                        ligne1pourPrint += "      ";
                    }
                }
                else if(bouffeQuantity < 100000)
                {
                    ligne1pourPrint += to_string(bouffeQuantity);
                    for(int iEspace = 0; iEspace < 5 - (int)log10(bouffeQuantity); iEspace++)
                    {
                        ligne1pourPrint += " ";
                    }
                }
                else // >= 100000
                {
                    ligne1pourPrint += ">10^5 ";
                }


                int nbrPheromones = map[i][j]->GetNbrPheromones();

                if(nbrPheromones < 0)
                {
                    ligne1pourPrint += "ERR";
                }
                else if(nbrPheromones == 0)
                {
                    ligne1pourPrint += "   ";
                }
                else if(nbrPheromones < 1000)
                {
                    for(int iEspace = 0; iEspace < 2 - (int)log10(nbrPheromones); iEspace++)
                    {
                        ligne1pourPrint += " ";
                    }
                    ligne1pourPrint += to_string(nbrPheromones);
                }
                else // >= 1000
                {
                    ligne1pourPrint += "AAA";
                }


                // --> Remplissons ensuite les 2 lignes représentant des jauges 

                // En premier lieu pour le nombre de fourmis
                int nbrFourmis = map[i][j]->GetNbrFourmisPresentes();

                ligne2pourPrint += couleurCase;
                ligne3pourPrint += couleurCase;

                ligne2pourPrint += "\e[33m";
                ligne3pourPrint += "\e[33m";


                if(nbrFourmis <= 0) //▁ ▂ ▃ ▄ ▅ ▆ ▇ █
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += " ";
                }
                else if(nbrFourmis == 1)       
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▁";
                }
                else if(nbrFourmis == 2)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▂";
                }
                else if(nbrFourmis == 3)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▃";
                }
                else if(nbrFourmis == 4)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▄";
                }
                else if(nbrFourmis == 5)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▅";
                }
                else if(nbrFourmis == 6)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▆";
                }
                else if(nbrFourmis == 7)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▇";
                }
                else if(nbrFourmis == 8)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 8)
                {
                    ligne2pourPrint += "▁";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 9)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 10)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 11)
                {
                    ligne2pourPrint += "▃";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 12)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 13)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 14)
                {
                    ligne2pourPrint += "▅";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 15)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 16)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 17)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 18)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis == 19)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                else if(nbrFourmis >= 20)
                {
                    ligne2pourPrint += couleurCasePleine + "█";
                    ligne3pourPrint += couleurCasePleine + "█";
                }
                
                ligne2pourPrint += couleurCase + "  ";
                ligne3pourPrint += couleurCase + "  ";

























                int nbrPheromonesCase = map[i][j]->GetNbrPheromones();

                int nbrPheromonesBouffe = map[i][j]->GetNbrPheromones(BOUFFE);
                int nbrPheromonesFourmilliere = map[i][j]->GetNbrPheromones(FOURMILLIERE);
                int nbrPheromonesAlerte = map[i][j]->GetNbrPheromones(ALERTE);






                //affichage pheromones alerte en rouge
                if(nbrPheromonesAlerte > 0)
                {
                    ligne3pourPrint += couleurPoliceFourmilliereMorte;
                }
                if(nbrPheromonesAlerte >= 8)
                {
                    ligne2pourPrint += couleurPoliceFourmilliereMorte;
                }

                if(nbrPheromonesAlerte <= 0) //▁ ▂ ▃ ▄ ▅ ▆ ▇ █
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += " ";
                }
                else if(nbrPheromonesAlerte == 1)       
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▁";
                }
                else if(nbrPheromonesAlerte == 2)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▂";
                }
                else if(nbrPheromonesAlerte == 3)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▃";
                }
                else if(nbrPheromonesAlerte == 4)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▄";
                }
                else if(nbrPheromonesAlerte == 5)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▅";
                }
                else if(nbrPheromonesAlerte == 6)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▆";
                }
                else if(nbrPheromonesAlerte == 7)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▇";
                }
                else if(nbrPheromonesAlerte == 8)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 8)
                {
                    ligne2pourPrint += "▁";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 9)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 10)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 11)
                {
                    ligne2pourPrint += "▃";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 12)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 13)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 14)
                {
                    ligne2pourPrint += "▅";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 15)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 16)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 17)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 18)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte == 19)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesAlerte >= 20)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                
               // ligne2pourPrint += couleurCase + " ";
                //ligne3pourPrint += couleurCase + " ";


                ligne2pourPrint += "\e[35m ";
                ligne3pourPrint += "\e[35m ";



                //affichage pheromones bouffe
                if(nbrPheromonesBouffe <= 0) //▁ ▂ ▃ ▄ ▅ ▆ ▇ █
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += " ";
                }
                else if(nbrPheromonesBouffe == 1)       
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▁";
                }
                else if(nbrPheromonesBouffe == 2)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▂";
                }
                else if(nbrPheromonesBouffe == 3)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▃";
                }
                else if(nbrPheromonesBouffe == 4)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▄";
                }
                else if(nbrPheromonesBouffe == 5)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▅";
                }
                else if(nbrPheromonesBouffe == 6)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▆";
                }
                else if(nbrPheromonesBouffe == 7)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▇";
                }
                else if(nbrPheromonesBouffe == 8)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 8)
                {
                    ligne2pourPrint += "▁";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 9)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 10)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 11)
                {
                    ligne2pourPrint += "▃";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 12)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 13)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 14)
                {
                    ligne2pourPrint += "▅";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 15)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 16)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 17)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 18)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe == 19)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesBouffe >= 20)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                
                ligne2pourPrint += couleurCase + " ";
                ligne3pourPrint += couleurCase + " ";





                //affichage pheromones fourmilliere
                if(nbrPheromonesFourmilliere <= 0) //▁ ▂ ▃ ▄ ▅ ▆ ▇ █
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += " ";
                }
                else if(nbrPheromonesFourmilliere == 1)       
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▁";
                }
                else if(nbrPheromonesFourmilliere == 2)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▂";
                }
                else if(nbrPheromonesFourmilliere == 3)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▃";
                }
                else if(nbrPheromonesFourmilliere == 4)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▄";
                }
                else if(nbrPheromonesFourmilliere == 5)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▅";
                }
                else if(nbrPheromonesFourmilliere == 6)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▆";
                }
                else if(nbrPheromonesFourmilliere == 7)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "▇";
                }
                else if(nbrPheromonesFourmilliere == 8)
                {
                    ligne2pourPrint += " ";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 8)
                {
                    ligne2pourPrint += "▁";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 9)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 10)
                {
                    ligne2pourPrint += "▂";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 11)
                {
                    ligne2pourPrint += "▃";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 12)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 13)
                {
                    ligne2pourPrint += "▄";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 14)
                {
                    ligne2pourPrint += "▅";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 15)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 16)
                {
                    ligne2pourPrint += "▆";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 17)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 18)
                {
                    ligne2pourPrint += "▇";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere == 19)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                else if(nbrPheromonesFourmilliere >= 20)
                {
                    ligne2pourPrint += "█";
                    ligne3pourPrint += "█";
                }
                
                ligne2pourPrint += couleurCase + " ";
                ligne3pourPrint += couleurCase + " ";
            }
        }

        // Dernieres petites mises au point avant d'afficher :
        ligne0pourPrint += "\n";
        if(i < LONGUEURMAP)
        {
            string couleurGrille = couleurGrilleNormale;
            if(map[i][LARGEURMAP - 1]->GetFourmilliereCase() != nullptr)
            {
            couleurGrille = couleurGrilleFourmilliereVivante;                                
            }                            
            ligne1pourPrint += couleurGrille + "│\n";
            ligne2pourPrint += couleurGrille + "│\n";
            ligne3pourPrint += couleurGrille + "│\n";
        }

        cout << ligne0pourPrint << ligne1pourPrint << ligne2pourPrint << ligne3pourPrint;
    }

    // je reset les couleurs
    cout << "\e[0m";
}

// pour changer de couleur l'écriture : 
//cout<<  "\e[32m" << "Salut\n";

// Définition de quelques codes de couleurs
// pour les terminaux ANSI :
/*
[0m			reset; clears all colors and styles (to white on black)
 
[30m		set foreground color to black
[31m		set foreground color to red
[32m		set foreground color to green
[33m		set foreground color to yellow
[34m		set foreground color to blue
[35m		set foreground color to magenta (purple)
[36m		set foreground color to cyan
[37m		set foreground color to white
[39m		set foreground color to default (white)

[40m		set background color to black
[41m		set background color to red
[42m		set background color to green
[43m		set background color to yellow
[44m		set background color to blue
[45m		set background color to magenta (purple)
[46m		set background color to cyan
[47m		set background color to white
[49m		set background color to default (black)
*/
//                                                                    █
//                                                                    █ 
//┼ ┤ ├ │    ¯  ─      ┐└ ┘┌   ┴ ┬    ░ ▒ ▓ █      ▁ ▂ ▃ ▄ ▅ ▆ ▇ █       ∰
//   ┌-┐
//   │ │  ¯¯¯¯____╱
//   └-┘
//
//
//          __¯¯__¯¯__¯¯__¯¯__
//         │  ││ ││  ││  ││ │
//         │  ││ ││  ││  ││ │
//        __¯¯__¯¯__¯¯__¯¯__¯¯
//       │  ││ ││  ││  ││ │
//       │  ││ ││  ││  ││ │
//        ¯¯  ¯¯  ¯¯  ¯¯  ¯¯
//
//
//        /¯¯\  /¯¯\   ╱¯¯\  /¯¯\/
//       │   │▓│   │▓│   │▓│   │
//       │   │▓│   │▓│   │▓│   │
//        \__/  \__/   \__/  \__/
//            │     │    │  │      │    │
//

