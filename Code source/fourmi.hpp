#ifndef FOURMI_HPP
#define FOURMI_HPP

#include "general.hpp"
#include "mersenneTwister.hpp"

#include "caseMap.hpp"


class CaseMap;


class Fourmi
{


    private:
        int indiceListeFourmisLoyalesFourmilliere;
        int indiceFourmilliere; // Pour savoir si les autres fourmis ou les phéromones sont amis.
        int positionFourmiSurCase; // si -1, alors ça veut dire qu'elle est dans une fourmilliere. 

        int carrylevel; 

        int jabotSocialLevel; 
        int ageFourmi;


        EtatFourmi etatFourmi;
        int nbrActionsDepuisAlerte;
        EtatFourmilliere etatFourmilliere;
        int jaugeDeFatigue; // C'est une sorte de jauge qui va diminuer le nombre de PA alloués chaque début de tour. Cette jauge est résuisibl uniquement en effectuant des actions d'inaction. 
        int pointsAction;

        CaseMap* caseActuelle;

        


    public :

        int testpheromonevar = 0;

        Fourmi(int, int, CaseMap*);

        ~Fourmi();


        bool toutEstOkDebutAction();

        void rechargerPointsActionDebutTour();
        DecisionFourmi debutActionReconnaissanceEtPriseDeDecision(); // Elle prend en compte l'environnement, (si sur une case) communique avec les fourmis présentes sur la case met à jour les infos et prend une décison.
            
        bool survieFourmi();
        void vieillirFourmi();

        void resterInactiveDansFourmilliere();
        void entrerDansFourmilliere();
        void sortirDeFourmilliere();
        void changerDeCase(Direction); // dès qu'elles bougent, elles communiquent automatiquement avec toutes les fourmis qui sont sur la case, notamment pour reconnaître les amies. L'ordre est aléatoire et elles ont une certaine probabilité de se transmettre leur état ou bien de partager leur bouffe.
        void ramasserBouffe();

        // communication entre fourmis
        void trophallaxie(int); // Action de partager son jabot social pendant la communication avec une autre fourmi. 
        void SetEtatFourmiALERTED(); // etatFourmi = alerted et reset le nbrActionsDepuisAlerte  


        int GetIndiceListeFourmisLoyalesFourmilliere();
        int GetIndiceFourmilliere();
        int GetPositionFourmiSurCase(); // si -1, alors dans fourmilliere
        int GetJabotSocialLevel();
        void SetJabotSocialLevel(int);
        int GetCarryLevel();
        void SetCarryLevel(int);
        int GetAgeFourmi();
        EtatFourmi GetEtatFourmi();

        int GetPointsActionfourmi();
        int GetNbrActionsDepuisAlerte();
        void SetNbrActionsDepuisalerte(int);

        CaseMap* GetCaseActuelle();
        
       
};



#endif
